<?
$MESS['CURRENCY_RATE_ADMIN_LIST_TITLE'] = 'Список курса валют';
$MESS['CURRENCY_RATE_ADMIN_LIST']       = 'Курсов валют';

$MESS['CURRENCY_RATE_ADMIN_ID']     = 'Id';
$MESS['CURRENCY_RATE_ADMIN_CODE']   = 'Символьный код';
$MESS['CURRENCY_RATE_ADMIN_COURSE'] = 'Курс';
$MESS['CURRENCY_RATE_ADMIN_DATE']   = 'Дата';

$MESS['EDIT']           = 'Редактировать';
$MESS['DELETE']         = 'Удалить';
$MESS['DELETE_CONFIRM'] = 'Подтвердить удаление';

$MESS['SAVE_ERROR']               = 'Ошибка сохранения';
$MESS['DELETE_ERROR']             = 'Ошибка удаления';
$MESS['DATE_FORMAT_FILTER_ERROR'] = 'Введите в фильтре правильную дату';
$MESS['INVALID_FIELD_ERROR']      = 'Введены некорректные данные';

$MESS['ADD_CURRENCY_RATE']       = 'Добавить курс валюты';
$MESS['ADD_CURRENCY_RATE_TITLE'] = 'Добавить новый курс валюты';