<?
$MESS['CURRENCY_RATE_TAB']       = 'Курс валюты';
$MESS['CURRENCY_RATE_TAB_TITLE'] = 'Параметры';

$MESS['CURRENCY_RATE_NEW_PAGE_TITLE']      = 'Новое значение курса валют';
$MESS['CURRENCY_RATE_EDIT_PAGE_TITLE']     = 'Курс валюты id = #ID#';
$MESS['CURRENCY_RATE_ERROR_SAVING']        = 'Ошибка сохранения!';
$MESS['CURRENCY_RATE_EMPTY_FIELD_ERROR']   = 'Заполните поле';
$MESS['CURRENCY_RATE_INVALID_FIELD_ERROR'] = 'Введены некорректные данные';

$MESS['CURRENCY_RATE_LIST']              = 'Список курсов валют';
$MESS['CURRENCY_RATE_LIST_TITLE']        = 'Перейти к списку курсов валют';
$MESS['CURRENCY_RATE_ADD']               = 'Добавить';
$MESS['CURRENCY_RATE_ADD_TITLE']         = 'Добавить новый курс валют';
$MESS['CURRENCY_RATE_DELETE']            = 'Удалить';
$MESS['CURRENCY_RATE_DELETE_TITLE']      = 'Удалить данный курс валют';
$MESS['CURRENCY_RATE_DELETE_TITLE_CONF'] = 'Вы уверенны что хотите удалить данный курс валют?';

$MESS['CURRENCY_RATE_ID_TITLE']     = 'Id';
$MESS['CURRENCY_RATE_CODE_TITLE']   = 'Символьный код';
$MESS['CURRENCY_RATE_COURSE_TITLE'] = 'Курс';
$MESS['CURRENCY_RATE_DATE_TITLE']   = 'Дата на которую курс был актуален';
