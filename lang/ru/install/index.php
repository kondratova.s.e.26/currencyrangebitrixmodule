<?
$MESS['KONDR_CURRENCY_RATE_MODULE_NAME']        = 'Курсы валют';
$MESS['KONDR_CURRENCY_RATE_MODULE_DESCRIPTION'] = 'Модуль для работы с курсами валют';
$MESS['KONDR_CURRENCY_RATE_PARTNER_NAME']       = 'Kondr';
$MESS['KONDR_CURRENCY_RATE_PARTNER_URI']        = '';

$MESS['KONDR_CURRENCY_RATE_INSTALL_ERROR_D7'] = 'Для работы модуля необходима версия Битрикс с поддержкой D7';
$MESS['KONDR_CURRENCY_RATE_INSTALL_TITLE']    = 'Установка модуля Курсы валют';
$MESS['KONDR_CURRENCY_RATE_UNINSTALL_TITLE']  = 'Удаление модуля Курсы валют';
