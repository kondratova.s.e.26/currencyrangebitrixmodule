<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc;

if (!check_bitrix_sessid()) {
    return;
}

if ($ex = $APPLICATION->GetException()) {
    $message = [
        'TYPE'    => 'ERROR',
        'MESSAGE' => Loc::getMessage('MOD_UNINST_ERROR'),
        'DETAILS' => $ex->GetString(),
        'HTML'    => true,
    ];
} else {
    $message = [
        'TYPE'    => 'OK',
        'MESSAGE' => Loc::getMessage('MOD_UNINST_OK'),
    ];
}
echo (new CAdminMessage($message))->Show();
?>

<form action="<?= $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <input type="submit" name="" value="<?= Loc::getMessage('MOD_BACK') ?>">
</form>
