<?

use Bitrix\Main\Localization\Loc;

class kondr_currencyrate extends CModule
{
    protected $ormClasses = [
        '\\Kondr\\Currencyrate\\CurrencyRateTable'
    ];

    private static $installDirectories = [
        'components',
        'admin',
    ];
    private static $removeComponents = [
        'currencyRate.list',
        'currencyRate.filter',
    ];
    private static $removeFiles = [
        'admin'
    ];
    private static $redirectFiles = [
        'currency_rate_admin.php',
        'currency_rate_edit.php'
    ];

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        $this->MODULE_ID           = 'kondr.currencyrate';
        $this->MODULE_VERSION      = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME         = Loc::getMessage('KONDR_CURRENCY_RATE_MODULE_NAME');
        $this->MODULE_DESCRIPTION  = Loc::getMessage('KONDR_CURRENCY_RATE_MODULE_DESCRIPTION');

        $this->PARTNER_NAME        = Loc::getMessage('KONDR_CURRENCY_RATE_PARTNER_NAME');
        $this->PARTNER_URI         = Loc::getMessage('KONDR_CURRENCY_RATE_PARTNER_URI');
    }

    public function DoInstall(): void
    {
        global $APPLICATION;

        if ($this->isVersionD7()) {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
            \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        } else {
            $APPLICATION->ThrowException(Loc::getMessage('KONDR_CURRENCY_RATE_INSTALL_ERROR_D7'));
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage('KONDR_CURRENCY_RATE_INSTALL_TITLE'),
            __DIR__ . '/step.php'
        );
    }

    public function DoUninstall(): void
    {
        global $APPLICATION;

        $context = \Bitrix\Main\Context::getCurrent();
        $request = $context->getRequest();

        if ($request['step'] < 2) {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('KONDR_CURRENCY_RATE_UNINSTALL_TITLE'),
                __DIR__ . '/unstep1.php'
            );
        } else {
            $this->UnInstallFiles();
            $this->UnInstallEvents();
            if ($request['savedata'] !== 'Y') {
                $this->UnInstallDB();
            }

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('KONDR_CURRENCY_RATE_UNISTALL_TITLE'),
                __DIR__ . '/unstep2.php'
            );
        }
    }

    public function InstallDB(): void
    {
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

        foreach ($this->ormClasses as $ormClass) {
            $instance = \Bitrix\Main\Entity\Base::getInstance($ormClass);

            if (!\Bitrix\Main\Application::getConnection()->isTableExists($instance->getDBTableName())) {
                $instance->createDbTable();
            }
        }
    }

    public function UnInstallDB()
    {
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);

        foreach ($this->ormClasses as $ormClass) {
            $instance = \Bitrix\Main\Entity\Base::getInstance($ormClass);

            \Bitrix\Main\Application::getConnection()->queryExecute(
                "DROP TABLE IF EXISTS {$instance->getDBTableName()}"
            );
        }
    }

    public function InstallFiles(): void
    {
        $this->fillAdminRedirectFiles();

        foreach (self::$installDirectories as $installDirectory) {
            CopyDirFiles(
                __DIR__ . '/' . $installDirectory,
                $_SERVER['DOCUMENT_ROOT'] . '/bitrix/' . $installDirectory,
                true,
                true
            );
        }
    }

    protected function fillAdminRedirectFiles(): void
    {
        $baseDir = str_contains(__DIR__, 'local/') ? '/local' : '/bitrix';

        foreach (self::$redirectFiles as $file) {
            $content = '<?
require($_SERVER["DOCUMENT_ROOT"] . "' . $baseDir . '/modules/kondr.currencyrate/admin/' . $file . '");
?>';
            \Bitrix\Main\IO\File::putFileContents(__DIR__ . "/admin/" . $file, $content);
        }
    }

    public function UnInstallFiles(): void
    {
        foreach (self::$removeComponents as $removeComponent) {
            DeleteDirFilesEx('/bitrix/components/kondr/' . $removeComponent);
        }

        foreach (self::$removeFiles as $removeFile) {
            DeleteDirFiles(
                __DIR__ . '/' . $removeFile,
                $_SERVER["DOCUMENT_ROOT"] . '/bitrix/' . $removeFile
            );
        }

        rmdir($_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/kondr');
    }

    protected function isVersionD7(): bool
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.0.0');
    }
}
