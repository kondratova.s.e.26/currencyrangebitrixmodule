<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
    'NAME'        => GetMessage('CURRENCY_RATE_LIST_NAME'),
    'DESCRIPTION' => GetMessage('CURRENCY_RATE_LIST_DESCRIPTION'),
    'SORT'        => 20,
    'PATH'        => [
        'ID'    => 'kondr',
        'NAME'  => GetMessage('KONDR'),
        'CHILD' => [
            'ID'   => 'kondr_currency_rate',
            'NAME' => GetMessage('KONDR_CURRENCY_RATE'),
            'SORT' => 20
        ]
    ],
    'CACHE_PATH'  => 'Y',
];
