<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Kondr\Currencyrate\CurrencyRate\CurrencyRateHelper;

class CurrencyRateList extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams): array
    {
        return [
            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
            'CACHE_TIME' => isset($arParams['CACHE_TIME']) ? $arParams['CACHE_TIME'] : 36000000,
            'SHOW_FILTER' => $arParams['SHOW_FILTER'] === 'Y',
            'DISPLAYED_COLUMNS_LIST' => $arParams['DISPLAYED_COLUMNS_LIST'] ?? [],
            'SHOW_NAV' => $arParams['SHOW_NAV'] === 'Y',
            'NAV_GET_PARAM' => $arParams['NAV_GET_PARAM'] ?? 'nav',
            'PAGE_ELEMENT_COUNT' => $arParams['PAGE_ELEMENT_COUNT'] ?? 10,
            'SORT_BY1' => $arParams['SORT_BY1'],
            'SORT_ORDER1' => $arParams['SORT_ORDER1'],
            'SORT_BY2' => $arParams['SORT_BY2'],
            'SORT_ORDER2' => $arParams['SORT_ORDER2'],
            'ADDITIONAL_FILTER' => $arParams['ADDITIONAL_FILTER'] ?? [],
            'PARAMS_FOR_AJAX' => [
                'CACHE_TIME',
                'DISPLAYED_COLUMNS_LIST',
                'SHOW_NAV',
                'SORT_BY1',
                'SORT_ORDER1',
                'SORT_BY2',
                'SORT_ORDER2',
                'NAV_GET_PARAM',
                'PAGE_ELEMENT_COUNT',
            ]
        ];
    }

    public function executeComponent(): void
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $navParam = $request->get($this->arParams['NAV_GET_PARAM']) ?? '';

        $additionalCacheID = [];
        if (!empty($this->arParams['ADDITIONAL_FILTER'])) {
            $additionalCacheID = $this->arParams['ADDITIONAL_FILTER'];
        }
        if (!empty($navParam)) {
            $additionalCacheID[] = $this->arParams['NAV_GET_PARAM'] . '=' . $navParam;
        }

        if($this->startResultCache(false, $additionalCacheID))
        {
            Loader::includeModule('kondr.currencyrate');

            $currencyRateHelper = (new CurrencyRateHelper())
                ->setParams($this->arParams);
            $this->arResult['currencyRates'] = $currencyRateHelper->getCurrencyRates();
            if ($this->arParams['SHOW_NAV']) {
                $this->arResult['nav'] = $currencyRateHelper->getNav();
            }

            $this->includeComponentTemplate();
        }
    }
}
