<?
/**
 * @global CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 * @var CurrencyRate[] $currencyRates
 * @var AdminPageNavigation $currencyRates
 * @var CBitrixComponent $component
 */

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Page\AssetLocation;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\AdminPageNavigation;
use Kondr\Currencyrate\CurrencyRate\CurrencyRate;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$currencyRates = $arResult['currencyRates'];
$nav = $arResult['nav']
?>
    <td class="main-column">
<? if(!$arParams['DISPLAYED_COLUMNS_LIST']) { ?>
    <p><? echo Loc::getMessage('CURRENCY_RATE_RESULT_COLUMNS_LIST_IS_EMPTY'); ?></p>
    <? return; ?>
<? } ?>
<? if(!$currencyRates) { ?>
    <p><? echo Loc::getMessage('CURRENCY_RATE_RESULT_NOT_FOUND'); ?></p>
    <? return; ?>
<? } ?>
<table>
    <tr>
        <? if(in_array('ID', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
        <th><? echo Loc::getMessage('CURRENCY_RATE_ID'); ?></th>
        <? } ?>
        <? if(in_array('CODE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
        <th><? echo Loc::getMessage('CURRENCY_RATE_CODE'); ?></th>
        <? } ?>
        <? if(in_array('DATE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
        <th><? echo Loc::getMessage('CURRENCY_RATE_DATE'); ?></th>
        <? } ?>
        <? if(in_array('COURSE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
        <th><? echo Loc::getMessage('CURRENCY_RATE_COURSE'); ?></th>
        <? } ?>
    </tr>
    <? foreach ($currencyRates as $currencyRate) { ?>
        <tr>
            <? if(in_array('ID', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
            <td><? echo $currencyRate->getId(); ?></td>
            <? } ?>
            <? if(in_array('CODE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
            <td><? echo $currencyRate->getCode(); ?></td>
            <? } ?>
            <? if(in_array('DATE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
            <td><? echo $currencyRate->getDateAsString() ?? ''; ?></td>
            <? } ?>
            <? if(in_array('COURSE', $arParams['DISPLAYED_COLUMNS_LIST'])) { ?>
            <td><? echo $currencyRate->getCourse() ?? ''; ?></td>
            <? } ?>
        </tr>
    <? } ?>
</table>
<?
if ($arParams['SHOW_NAV']) {
    $APPLICATION->IncludeComponent(
        'bitrix:main.pagenavigation',
        '.default',
        array(
            'NAV_OBJECT' => $nav,
            'SEF_MODE'   => 'N',
        ),
        $component
    );
}
?>
    </td>
<?

$params = Json::encode(array_intersect_key($arParams, array_flip($arParams['PARAMS_FOR_AJAX'])));
$string = <<<HTML
<script>
    window.Kondr = window.Kondr || {};
    window.Kondr.currencyRate = window.Kondr.currencyRate || {};
    window.Kondr.currencyRate.list = window.Kondr.currencyRate.list || {};
   
    window.Kondr.currencyRate.list = {
        params: {$params}
        };
    window.Kondr.currencyRate.list.getList = function (filters) {
        return new Promise(function(resolve, reject) {
            BX.ajax.runComponentAction('kondr:currencyRate.list', 'getList', {
                mode: 'ajax',
                data: {
                    params: window.Kondr.currencyRate.list.params,
                    additionalFilters: filters
                }
            }).then(function (response) {
                resolve();
            }.bind(this), function (response) {
                reject();
            });
        });
    }
</script>
HTML;
Asset::getInstance()->addString($string, false, AssetLocation::AFTER_JS);
