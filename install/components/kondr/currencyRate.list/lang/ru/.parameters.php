<?
$MESS['CURRENCY_RATE_ADDITIONAL']              = 'Дополнительно';
$MESS['CURRENCY_RATE_SORT']                    = 'Сортировка';
$MESS['CURRENCY_RATE_NAV']                     = 'Навигация';
$MESS ['CURRENCY_RATE_SHOW_FILTER']            = 'Показывать фильтр';
$MESS ['CURRENCY_RATE_DISPLAYED_COLUMNS_LIST'] = 'Отображаемые столбцы в списке';
$MESS ['CURRENCY_RATE_SHOW_NAV']               = 'Отображать постраничную навигацию';
$MESS ['CURRENCY_RATE_NAV_GET_PARAM']          = 'Параметр навигации в строке запроса';
$MESS ['CURRENCY_RATE_PAGE_ELEMENT_COUNT']     = 'Количество элементов на странице';

$MESS['CURRENCY_RATE_ID']     = 'Id';
$MESS['CURRENCY_RATE_CODE']   = 'Код';
$MESS['CURRENCY_RATE_DATE']   = 'Дата';
$MESS['CURRENCY_RATE_COURSE'] = 'Курс';

$MESS ['CURRENCY_RATE_SORT_ASC']          = 'По возрастанию';
$MESS ['CURRENCY_RATE_SORT_DESC']         = 'По убыванию';
$MESS ['CURRENCY_RATE_SORT_FIELD_ID']     = 'Id';
$MESS ['CURRENCY_RATE_SORT_FIELD_CODE']   = 'Символьный код валюты';
$MESS ['CURRENCY_RATE_SORT_FIELD_DATE']   = 'Дата записи курса';
$MESS ['CURRENCY_RATE_SORT_FIELD_COURSE'] = 'Курс валюты';
$MESS ['CURRENCY_RATE_SORT_ORD1']         = 'Поле для первой сортировки';
$MESS ['CURRENCY_RATE_SORT_BY1']          = 'Направление для первой сортировки';
$MESS ['CURRENCY_RATE_SORT_ORD2']         = 'Поле для второй сортировки';
$MESS ['CURRENCY_RATE_SORT_BY2']          = 'Направление для второй сортировки';
