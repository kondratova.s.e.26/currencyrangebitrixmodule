<?
$MESS['CURRENCY_RATE_ID']     = 'Id';
$MESS['CURRENCY_RATE_CODE']   = 'Символьный код валюты';
$MESS['CURRENCY_RATE_DATE']   = 'Дата';
$MESS['CURRENCY_RATE_COURSE'] = 'Курс';

$MESS['CURRENCY_RATE_RESULT_NOT_FOUND']             = 'Результат не найден';
$MESS['CURRENCY_RATE_RESULT_COLUMNS_LIST_IS_EMPTY'] = 'Колонки не выбраны';
