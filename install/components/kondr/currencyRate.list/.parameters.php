<?

$columns = [
    'ID'     => GetMessage('CURRENCY_RATE_ID'),
    'CODE'   => GetMessage('CURRENCY_RATE_CODE'),
    'DATE'   => GetMessage('CURRENCY_RATE_DATE'),
    'COURSE' => GetMessage('CURRENCY_RATE_COURSE'),
];

$sorts      = [
    'ASC'  => GetMessage('CURRENCY_RATE_SORT_ASC'),
    'DESC' => GetMessage('CURRENCY_RATE_SORT_DESC')
];
$sortFields = [
    'ID'     => GetMessage('CURRENCY_RATE_SORT_FIELD_ID'),
    'CODE'   => GetMessage('CURRENCY_RATE_SORT_FIELD_CODE'),
    'DATE'   => GetMessage('CURRENCY_RATE_SORT_FIELD_DATE'),
    'COURSE' => GetMessage('CURRENCY_RATE_SORT_FIELD_COURSE')
];

$arComponentParameters = [
    'GROUPS' => [
        'SORT' => [
            'NAME' => GetMessage('CURRENCY_RATE_SORT')
        ],
        'NAV' => [
            'NAME' => GetMessage('CURRENCY_RATE_NAV')
        ],
        'ADDITIONAL' => [
            'NAME' => GetMessage('CURRENCY_RATE_ADDITIONAL')
        ],
    ],
    'PARAMETERS' => [
        'CACHE_TIME' => [],
        'SHOW_FILTER' => [
            'PARENT'   => 'ADDITIONAL',
            'NAME'     => GetMessage('CURRENCY_RATE_SHOW_FILTER'),
            'TYPE'     => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'DEFAULT'  => 'N',
        ],
        'DISPLAYED_COLUMNS_LIST' => [
            'PARENT'   => 'ADDITIONAL',
            'NAME'     => GetMessage('CURRENCY_RATE_DISPLAYED_COLUMNS_LIST'),
            'TYPE'     => 'LIST',
            'MULTIPLE' => 'Y',
            'DEFAULT'  => array_keys($columns),
            'VALUES'   => $columns,
        ],
        'SHOW_NAV' => [
            'PARENT'   => 'NAV',
            'NAME'     => GetMessage('CURRENCY_RATE_SHOW_NAV'),
            'TYPE'     => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'DEFAULT'  => 'N',
        ],
        'NAV_GET_PARAM' => array(
            'PARENT'  => 'NAV',
            'NAME'    => GetMessage('CURRENCY_RATE_NAV_GET_PARAM'),
            'TYPE'    => 'STRING',
            'DEFAULT' => 'nav'
        ),
        'PAGE_ELEMENT_COUNT' => array(
            'PARENT'  => 'NAV',
            'NAME'    => GetMessage('CURRENCY_RATE_PAGE_ELEMENT_COUNT'),
            'TYPE'    => 'STRING',
            'DEFAULT' => '5'
        ),
        'SORT_BY1' => [
            'PARENT'            => 'SORT',
            'NAME'              => GetMessage('CURRENCY_RATE_SORT_ORD1'),
            'TYPE'              => 'LIST',
            'DEFAULT'           => 'ID',
            'VALUES'            => $sortFields,
            'ADDITIONAL_VALUES' => 'Y',
        ],
        'SORT_ORDER1' => [
            'PARENT'  => 'SORT',
            'NAME'    => GetMessage('CURRENCY_RATE_SORT_BY1'),
            'TYPE'    => 'LIST',
            'DEFAULT' => 'DESC',
            'VALUES'  => $sorts,
        ],
        'SORT_BY2' => [
            'PARENT'            => 'SORT',
            'NAME'              => GetMessage('CURRENCY_RATE_SORT_ORD2'),
            'TYPE'              => 'LIST',
            'DEFAULT'           => 'DATE',
            'VALUES'            => $sortFields,
            'ADDITIONAL_VALUES' => 'Y',
        ],
        'SORT_ORDER2' => [
            'PARENT'  => 'SORT',
            'NAME'    => GetMessage('CURRENCY_RATE_SORT_BY2'),
            'TYPE'    => 'LIST',
            'DEFAULT' => 'ASC',
            'VALUES'  => $sorts,
        ],
    ]
];
