<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Loader;
use Kondr\Currencyrate\CurrencyRate\AjaxCache;
use Kondr\Currencyrate\CurrencyRate\AjaxResponse;
use Kondr\Currencyrate\CurrencyRate\CurrencyRateHelper;

class CurrencyRateListAjaxController extends Controller
{
    public function configureActions()
    {
        return [
            'getList' => [
                'prefilters' => [
                    new Csrf(),
                ],
                'postfilters' => [],
            ],
        ];
    }

    public function getListAction(array $params, array $additionalFilters = [])
    {
        try {
            Loader::includeModule('kondr.currencyrate');

            $currencyRateHelper = new CurrencyRateHelper();
            $params['ADDITIONAL_FILTER'] = $currencyRateHelper->prepareAdditionalFilters($additionalFilters);
            $currencyRateHelper->setParams($params);

            $ajaxCache = (new AjaxCache())
                ->setComponentName($this->getSaltToUnsign())
                ->createCacheId($params['ADDITIONAL_FILTER']);
            $obCache = new CPHPCache();

            if ($obCache->InitCache($params['CACHE_TIME'], $ajaxCache->getCacheId(), $ajaxCache->getPath()))
            {
                $result = $obCache->GetVars();
            }
            elseif ($obCache->StartDataCache())
            {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($ajaxCache->getPath());

                $result['currencyRates'] = $currencyRateHelper->getCurrencyRates();

                $CACHE_MANAGER->EndTagCache();
                $obCache->EndDataCache($result);
            }

        } catch (Throwable $exception) {
            if(!empty($obCache)) {
                $obCache->AbortDataCache();
            }

            return AjaxResponse::getFailureResponse()
                ->withNotifyMessages([
                    ['text' => $exception->getMessage(), 'type' => 'error'],
                ]);
        }

        $ajaxResponse= $currencyRateHelper->prepareAjaxResponse($result['currencyRates']);

        return AjaxResponse::getSuccessResponse()
            ->withPayload($ajaxResponse);
    }
}
