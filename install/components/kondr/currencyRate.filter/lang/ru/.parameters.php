<?
$MESS['CURRENCY_RATE_ADDITIONAL']         = 'Дополнительно';
$MESS ['CURRENCY_RATE_DISPLAYED_FILTERS'] = 'Отображаемые фильтры';

$MESS['CURRENCY_RATE_CODE']   = 'Код';
$MESS['CURRENCY_RATE_DATE']   = 'Дата';
$MESS['CURRENCY_RATE_COURSE'] = 'Курс';

$MESS['CURRENCY_RATE_FILTER_CODE_NAME']   = 'Отображаемое название фильтра Код в шаблоне';
$MESS['CURRENCY_RATE_FILTER_DATE_NAME']   = 'Отображаемое название фильтра Дата в шаблоне';
$MESS['CURRENCY_RATE_FILTER_COURSE_NAME'] = 'Отображаемое название фильтра Курс в шаблоне';

$MESS['CURRENCY_RATE_FILTER_CODE_DEFAULT_NAME']   = 'Код';
$MESS['CURRENCY_RATE_FILTER_DATE_DEFAULT_NAME']   = 'Дата';
$MESS['CURRENCY_RATE_FILTER_COURSE_DEFAULT_NAME'] = 'Курс';
