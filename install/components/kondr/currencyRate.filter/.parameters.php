<?

$filters = [
    'CODE'   => GetMessage('CURRENCY_RATE_CODE'),
    'COURSE' => GetMessage('CURRENCY_RATE_COURSE'),
    'DATE'   => GetMessage('CURRENCY_RATE_DATE'),
];

$arComponentParameters = [
    'GROUPS' => [
        'ADDITIONAL' => [
            'NAME' => GetMessage('CURRENCY_RATE_ADDITIONAL')
        ],
    ],
    'PARAMETERS' => [
        'CACHE_TIME' => [],
        'DISPLAYED_FILTERS' => [
            'PARENT'   => 'ADDITIONAL',
            'NAME'     => GetMessage('CURRENCY_RATE_DISPLAYED_FILTERS'),
            'TYPE'     => 'LIST',
            'MULTIPLE' => 'Y',
            'DEFAULT'  => array_keys($filters),
            'VALUES'   => $filters,
        ],
        'CURRENCY_RATE_FILTER_CODE_NAME' => array(
            'PARENT'  => 'ADDITIONAL',
            'NAME'    => GetMessage('CURRENCY_RATE_FILTER_CODE_NAME'),
            'TYPE'    => 'STRING',
            'DEFAULT' => GetMessage('CURRENCY_RATE_FILTER_CODE_DEFAULT_NAME')
        ),
        'CURRENCY_RATE_FILTER_COURSE_NAME' => array(
            'PARENT'  => 'ADDITIONAL',
            'NAME'    => GetMessage('CURRENCY_RATE_FILTER_COURSE_NAME'),
            'TYPE'    => 'STRING',
            'DEFAULT' => GetMessage('CURRENCY_RATE_FILTER_COURSE_DEFAULT_NAME')
        ),
        'CURRENCY_RATE_FILTER_DATE_NAME' => array(
            'PARENT'  => 'ADDITIONAL',
            'NAME'    => GetMessage('CURRENCY_RATE_FILTER_DATE_NAME'),
            'TYPE'    => 'STRING',
            'DEFAULT' => GetMessage('CURRENCY_RATE_FILTER_DATE_DEFAULT_NAME')
        ),
    ]
];
