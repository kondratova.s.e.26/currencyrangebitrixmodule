<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Kondr\Currencyrate\CurrencyRate\AjaxCache;
use Kondr\Currencyrate\CurrencyRate\Filter\FilterHelper;
use Kondr\Currencyrate\CurrencyRate\AjaxResponse;

class CurrencyRateFilterAjaxController extends Controller
{
    public function configureActions()
    {
        return [
            'get' => [
                'prefilters' => [
                    new Csrf(),
                ],
                'postfilters' => [],
            ],
        ];
    }

    public function getAction(): AjaxResponse
    {
        try {
            Loader::includeModule('kondr.currencyrate');

            $request = Application::getInstance()->getContext()->getRequest();
            $displayedFilter = $request->get('displayedFilter');
            $cacheTime = $request->get('cacheTime') ?? 0;

            $filterHelper    = new FilterHelper();
            $selectedFilters = $filterHelper->getSelectedFilters($displayedFilter);

            $ajaxCache = (new AjaxCache())
                ->setComponentName($this->getSaltToUnsign())
                ->createCacheId($selectedFilters);
            $obCache = new CPHPCache();

            if ($obCache->InitCache($cacheTime, $ajaxCache->getCacheId(), $ajaxCache->getPath()))
            {
                $result = $obCache->GetVars();
            }
            elseif ($obCache->StartDataCache())
            {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($ajaxCache->getPath());

                $result['filters'] = $filterHelper->getFilters($selectedFilters, $displayedFilter);

                $CACHE_MANAGER->EndTagCache();
                $obCache->EndDataCache($result);
            }
        } catch (Throwable $exception) {
            if(!empty($obCache)) {
                $obCache->AbortDataCache();
            }

            return AjaxResponse::getFailureResponse()
                ->withNotifyMessages([
                    ['text' => $exception->getMessage(), 'type' => 'error'],
                ]);
        }

        $ajaxFilters = $filterHelper->prepareAjaxResponse($result['filters']);

        return AjaxResponse::getSuccessResponse()
            ->withPayload([
                'filters' => $ajaxFilters
            ]);
    }
}
