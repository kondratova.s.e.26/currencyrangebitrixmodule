<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Kondr\Currencyrate\CurrencyRate\Filter\FilterHelper;
use \Kondr\Currencyrate\CurrencyRate\Filter\RangeFilterInterface;
use \Kondr\Currencyrate\CurrencyRate\Filter\ListFilterInterface;

class CurrencyRateFilter extends CBitrixComponent
{
    protected ErrorCollection $errorCollection;

    public function onPrepareComponentParams($arParams): array
    {
        $this->errorCollection = new ErrorCollection();

        return [
            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
            'CACHE_TIME' => isset($arParams['CACHE_TIME']) ? $arParams['CACHE_TIME'] : 36000000,
            'DISPLAYED_FILTERS' => $arParams['DISPLAYED_FILTERS'] ?? [],
            'CURRENCY_RATE_FILTER_NAMES' => [
                'CODE' => $arParams['CURRENCY_RATE_FILTER_CODE_NAME'] ?? '',
                'COURSE' => $arParams['CURRENCY_RATE_FILTER_COURSE_NAME'] ?? '',
                'DATE' => $arParams['CURRENCY_RATE_FILTER_DATE_NAME'] ?? '',
            ]
        ];
    }

    public function executeComponent(): array
    {
        Loader::includeModule('kondr.currencyrate');

        $filterHelper = new FilterHelper();
        $selectedFilters = $filterHelper->getSelectedFilters($this->arParams['DISPLAYED_FILTERS']);
        $additionalCacheID = $this->getAdditionalCacheID($selectedFilters);

        if($this->startResultCache(false, $additionalCacheID))
        {
            $this->arResult['filters'] = $filterHelper->getFilters(
                $selectedFilters,
                $this->arParams['DISPLAYED_FILTERS']
            );

            $this->includeComponentTemplate();

        }

        return $selectedFilters;
    }

    private function getAdditionalCacheID(array $selectedFilters)
    {
        $additionalCacheID = [];

        foreach ($selectedFilters as $selectedFilter) {
            switch (true) {
                case $selectedFilter instanceof RangeFilterInterface:
                    if (!!$selectedFilter->getFrom()) {
                        $additionalCacheID[$selectedFilter->getName() . 'FROM'] = $selectedFilter->getFromAsString();
                    }
                    if (!!$selectedFilter->getTo()) {
                        $additionalCacheID[$selectedFilter->getName() . 'TO'] = $selectedFilter->getToAsString();
                    }
                    break;
                case $selectedFilter instanceof ListFilterInterface:
                    if (!!$selectedFilter->getSelected()) {
                        $additionalCacheID[$selectedFilter->getName() . 'IN'] = $selectedFilter->getSelectedAsString();
                    }
                    break;
            }
        }

        return $additionalCacheID;
    }
}
