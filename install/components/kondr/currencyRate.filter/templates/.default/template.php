<?
/**
 * @global CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 * @var array $filters
 */

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Page\AssetLocation;
use Bitrix\Main\Web\Json;
use Kondr\Currencyrate\CurrencyRate\Filter\ListFilterInterface;
use Kondr\Currencyrate\CurrencyRate\Filter\RangeFilterInterface;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$filters = $arResult['filters'];

if (!$arParams['DISPLAYED_FILTERS']) {
    ?>
 Не выбраны фильтры для показа

<?
return;
}
?>
<td class="left-column">
<div>
    <form id="currencyRateFilter">
        <table>
            <td class="left-column">
                <ul class="left-menu">
                    <?
                    foreach ($filters as $filter) {
                        switch (true) {
                            case $filter instanceof RangeFilterInterface:
                                ?>
                                <li>
                                    <p><?= $arParams['CURRENCY_RATE_FILTER_NAMES'][$filter->getName()]; ?> от</p>
                                    <input
                                            type="text"
                                            name="<?= $filter->getName(); ?>min"
                                            value="<?= $filter->getFromAsString(); ?>"
                                            size="15"
                                            maxlength="50">
                                    <p><?= $arParams['CURRENCY_RATE_FILTER_NAMES'][$filter->getName()]; ?> до</p>
                                    <input
                                            type="text"
                                            name="<?= $filter->getName(); ?>max"
                                            value="<?= $filter->getToAsString(); ?>"
                                            size="15"
                                            maxlength="50">
                                </li>

                                <?
                                break;
                            case $filter instanceof ListFilterInterface:
                                ?>
                                <li>
                                    <p><?= $arParams['CURRENCY_RATE_FILTER_NAMES'][$filter->getName()]; ?></p>
                                    <? foreach ($filter->getAll() as $value) {
                                        $selected = $filter->getSelected();
                                        $check = in_array($value, $filter->getSelected());?>
                                    <input
                                            type="checkbox"
                                            id="<?= $value; ?>"
                                            name="<?= $filter->getName() . $value; ?>"
                                            value="<?= $value; ?>"
                                            <? if (in_array($value, $filter->getSelected())) { ?> checked <? }?>>
                                    <label for="<?= $value ?>"> <?= $value ?></label><br>
                                    <? }?>
                                </li>
                                <?
                                break;
                        }
                    }
                    ?>
                    <li>
                        <input type="submit" value="Применить">
                    </li>
                </ul>
            </td>
        </table>
    </form>
</div>
</td>
<?
$displayedFilter = Json::encode($arParams["DISPLAYED_FILTERS"]);
$cacheTime = Json::encode($arParams["CACHE_TIME"]);
$string = <<<HTML
<script>
    window.Kondr = window.Kondr || {};
    window.Kondr.currencyRate = window.Kondr.currencyRate || {};
    window.Kondr.currencyRate.filter = window.Kondr.currencyRate.filter || {};
    
    window.Kondr.currencyRate.filter.get = function (data) {
        data.displayedFilter = {$displayedFilter};
        data.cacheTime = {$cacheTime};
        return new Promise(function(resolve, reject) {
            BX.ajax.runComponentAction('kondr:currencyRate.filter', 'get', {
                mode: 'ajax',
                data: data
            }).then(function (response) {
                resolve(response);
            }.bind(this), function (response) {
                reject(response);
            });
        });
    }
</script>
HTML;
Asset::getInstance()->addString($string, false, AssetLocation::AFTER_JS);
?>

<script>
    const formElement = document.getElementById('currencyRateFilter');
    formElement.addEventListener('submit', (e) => {
        e.preventDefault();
        const formData = new FormData(formElement);
        const all = formData.getAll('min');

        // магия фронтенда которй заниматься сейчас я не буду
        // симуляция ответа фронта

        window.currencyRate = window.currencyRate || {};
        currencyRate.filter = {
            code: [
                'USD',
                'RUB'
            ],
            courseFrom: 10,
            courseTo: 125,
            dateFrom: '13.03.2024',
            dateTo: '30.03.2024',
        };

        // ajax обновление фильтра
        window.Kondr.currencyRate.filter.get(currencyRate.filter)
            .then(function (response) {
            // ajax обновление списка
            if (response.data && !!response.data.success) {
                window.Kondr.currencyRate.list.getList(response.data.payload.filters)
                    .then(function (response) {
                    }.bind(this), function (response) {
                    });
            }
        }.bind(this), function (response) {
        });
    });
        </script>
