<?php
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Loader;
use \Kondr\Currencyrate\CurrencyRateTable;
use \Kondr\Currencyrate\CurrencyRate\Admin\AdminListPage;
use \Kondr\Currencyrate\CurrencyRate\Admin\AdminListColumn;
use \Kondr\Currencyrate\CurrencyRate\Admin\AdminListColumnNumber;
use \Kondr\Currencyrate\CurrencyRate\Admin\AdminListColumnDate;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

Loader::includeModule('kondr.currencyrate');

$POST_RIGHT = $APPLICATION->GetGroupRight('kondr.currencyrate');

if ($POST_RIGHT == 'D') {
    $APPLICATION->AuthForm(getMessage('ACCESS_DENIED'));
}

$editPage = 'currency_rate_edit';
$ormTable = new CurrencyRateTable();
$menu = [
    [
        'TEXT'  => GetMessage('ADD_CURRENCY_RATE'),
        'LINK'  => $editPage . '.php?lang=' . LANG,
        'TITLE' => GetMessage('ADD_CURRENCY_RATE_TITLE'),
        'ICON'  => 'btn_new'
    ],
];

$listPage = (new AdminListPage())
    ->setOrmTable($ormTable)
    ->setPageTitle(GetMessage('CURRENCY_RATE_ADMIN_LIST_TITLE') ?? '')
    ->setNavLabel(GetMessage('CURRENCY_RATE_ADMIN_LIST') ?? '')
    ->setDateFormatError(GetMessage('DATE_FORMAT_FILTER_ERROR') ?? '')
    ->setSaveError(GetMessage('SAVE_ERROR') ?? '')
    ->setDeleteError(GetMessage('DELETE_ERROR') ?? '')
    ->setInvalidFieldError(GetMessage('INVALID_FIELD_ERROR') ?? '')
    ->setEditAction(GetMessage('EDIT') ?? '')
    ->setDeleteAction(GetMessage('DELETE') ?? '')
    ->setDeleteActionConf(GetMessage('DELETE_CONFIRM') ?? '')
    ->setRight($POST_RIGHT)
    ->setEditPage($editPage)
    ->setMenu($menu);

$listPage->addColumn(new AdminListColumn(
    'ID',
    'view',
    [
        'content'    => GetMessage('CURRENCY_RATE_ADMIN_ID'),
        'align'      => 'right',
        'sort'       => 'ID',
        'filter'     => 'find_id',
        'find_type'  => 'id',
        'filter_key' => '=ID',
        'default'    => true,
    ]
));
$listPage->addColumn(new AdminListColumn(
    'CODE',
    'input',
    [
        'content'    => GetMessage('CURRENCY_RATE_ADMIN_CODE'),
        'align'      => 'right',
        'sort'       => 'CODE',
        'filter'     => 'find_code',
        'find_type'  => 'code',
        'filter_key' => '=CODE',
        'default'    => true,
    ],
    true
));
$listPage->addColumn(new AdminListColumnNumber(
    'COURSE',
    'input',
    [
        'content'    => GetMessage('CURRENCY_RATE_ADMIN_COURSE'),
        'sort'       => 'COURSE',
        'filter'     => 'find_course',
        'filter_key' => '=COURSE',
        'default'    => true,
    ],
    true
));
$listPage->addColumn(new AdminListColumnDate(
    'DATE',
    'calendar',
    [
        'content'    => GetMessage('CURRENCY_RATE_ADMIN_DATE'),
        'sort'       => 'DATE',
        'filter'     => 'find_date',
        'filter_key' => '=DATE',
        'default'    => true,
    ],
    true
));

$listPage->init();

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$listPage->show();

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
