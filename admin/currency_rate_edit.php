<?php
/**
 * @global \CUser $USER
 * @global \CMain $APPLICATION
 */

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Kondr\Currencyrate\CurrencyRate\Admin\AdminEditPage;
use Kondr\Currencyrate\CurrencyRate\Admin\AdminEditPageField;
use Kondr\Currencyrate\CurrencyRate\Admin\AdminEditPageFieldInput;
use Kondr\Currencyrate\CurrencyRate\Admin\AdminEditPageFieldDate;
use Kondr\Currencyrate\CurrencyRateTable;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
require_once($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/prolog.php');

Loader::includeModule('kondr.currencyrate');

$right = $APPLICATION->GetGroupRight('kondr.currencyrate');

if($right == 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$listPage = 'currency_rate_admin';
$editPage = 'currency_rate_edit';
$ormTable = new CurrencyRateTable();
$request = Application::getInstance()->getContext()->getRequest();
$id = !empty($request->get('ID')) ? $request->get('ID') : null;
$fields = [
    'ID' => new AdminEditPageField(
        'ID',
        GetMessage('CURRENCY_RATE_ID_TITLE') ?? '',
        $id ?? '',
        true,
        empty($id)
    ),
    'CODE' => new AdminEditPageFieldInput(
        'CODE',
        GetMessage('CURRENCY_RATE_CODE_TITLE') ?? '',
        $request->get('CODE') ?? '',
        true,
    ),
    'COURSE' => new AdminEditPageFieldInput(
        'COURSE',
        GetMessage('CURRENCY_RATE_COURSE_TITLE') ?? '',
        $request->get('COURSE') ?? '',
        true,
    ),
    'DATE' => new AdminEditPageFieldDate(
        'DATE',
        GetMessage('CURRENCY_RATE_DATE_TITLE') ?? '',
        $request->get('DATE') ?? '',
        true,
    ),
];

$tabs = [
    [
        'DIV' => 'tub1',
        'TAB' => GetMessage('CURRENCY_RATE_TAB') ?? '',
        'ICON' => 'main_user_edit',
        'TITLE' => GetMessage('CURRENCY_RATE_TAB_TITLE') ?? '',
    ]
];
$menu[] = [
    'TEXT' => GetMessage('CURRENCY_RATE_LIST'),
    'LINK' => '/bitrix/admin/' . $listPage . '.php?lang=' . LANG,
    'ICON' => 'btn_list',
    'TITLE' => GetMessage('CURRENCY_RATE_LIST_TITLE'),
];

if (!empty($id)) {
    $menu[] = ['SEPARATOR' => 'Y'];
    $menu[] = [
        'TEXT' => GetMessage('CURRENCY_RATE_ADD'),
        'LINK' => '/bitrix/admin/' . $editPage . '.php?lang=' . LANG,
        'ICON' => 'btn_new',
        'TITLE' => GetMessage('CURRENCY_RATE_ADD_TITLE'),
    ];
    $menu[] = [
        'TEXT' => GetMessage('CURRENCY_RATE_DELETE'),
        'LINK' => "javascript:if(confirm('" . GetMessage('CURRENCY_RATE_DELETE_TITLE_CONF')
            . "')) window.location='/bitrix/admin/' . $listPage . '.php?action=delete&ID=" . $id
            . '&lang=' . LANGUAGE_ID . '&' . bitrix_sessid_get() . "';",
        'ICON' => 'btn_delete',
        'TITLE' => GetMessage('CURRENCY_RATE_DELETE_TITLE'),
    ];
}

$adminEditPage = (new AdminEditPage())
    ->setId($id)
    ->setFields($fields)
    ->setRight($right)
    ->setEditPage($editPage)
    ->setListPage($listPage)
    ->setEditPageTitle(GetMessage('CURRENCY_RATE_EDIT_PAGE_TITLE') ?? '')
    ->setAddPageTitle(GetMessage('CURRENCY_RATE_NEW_PAGE_TITLE') ?? '')
    ->setSaveErrorText(GetMessage('CURRENCY_RATE_ERROR_SAVING') ?? '')
    ->setEmptyFieldErrorText(GetMessage('CURRENCY_RATE_EMPTY_FIELD_ERROR') ?? '')
    ->setInvalidFieldErrorText(GetMessage('CURRENCY_RATE_INVALID_FIELD_ERROR') ?? '')
    ->setOrmTable($ormTable)
    ->setTabs($tabs)
    ->setMenu($menu);

$adminEditPage->init();

require($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/include/prolog_admin_after.php');

$adminEditPage->show();

require($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/include/epilog_admin.php');
