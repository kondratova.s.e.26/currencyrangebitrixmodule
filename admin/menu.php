<?php

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

$menu = [];

if ($USER->isAdmin()) {
    $menu[] = [
        'parent_menu' => 'global_menu_settings',
        'sort'        => 10,
        'text'        => getMessage('CURRENCY_RATE_ADMIN_MENU'),
        'title'       => GetMessage('CURRENCY_RATE_ADMIN_MENU_TITLE'),
        'url'         => 'currency_rate_admin.php?lang=' . LANGUAGE_ID,
        'items_id'    => 'currencyrate',
        'module_id'   => 'kondr.currencyrate',
    ];
}

return $menu;
