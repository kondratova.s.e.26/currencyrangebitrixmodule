<?php

namespace Kondr\Currencyrate;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * ОРМ-класс для таблицы курсов валют
 *
 * Поля:
 * <ul>
 * <li> ID int        - ИД записи
 * <li> CODE string   - Символьный код валюты
 * <li> DATE datetime - Дата и время курса
 * <li> COURSE float  - Курс
 * </ul>
 **/
class CurrencyRateTable extends Entity\DataManager
{
    public static function getTableName(): string
    {
        return 'kondr_currency_rate';
    }

    public static function getUfId(): string
    {
        return 'CURRENCY_RATE';
    }

    public static function getMap(): array
    {
        return [
            'ID' => new Entity\IntegerField('ID', [
                'autocomplete' => true,
                'primary' => true,
            ]),
            'CODE' => new Entity\StringField('CODE', [
                'required' => true,
            ]),
            'DATE' => new Entity\DateField('DATE', [
                'default_value' => new DateTime(),
                'required' => true,
            ]),
            'COURSE' => new Entity\FloatField('COURSE', [
                'required' => true,
            ]),
        ];
    }
}
