<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

use Bitrix\Main\Type\Date;

class CourseOnDateParamsBuilder
{
    public function build(Date $date): RequestParams
    {
        $request = '<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetCursOnDate xmlns="http://web.cbr.ru/">
      <On_date>' . $date->format('Y-m-d') . '</On_date>
    </GetCursOnDate>
  </soap12:Body>
</soap12:Envelope>';

        $requestParams = new RequestParams();

        $requestParams->url = 'http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?wsdl';
        $requestParams->headers = [
            'Content-type: text/xml;charset=\"utf-8\"',
            'Accept: text/xml',
            "SOAPAction: \"http://web.cbr.ru/GetCursOnDate\"",
            'Content-length: ' . strlen($request),
        ];
        $requestParams->body = $request;
        $requestParams->socketTimeout = 30;
        $requestParams->streamTimeout = 60;

        return $requestParams;
    }
}
