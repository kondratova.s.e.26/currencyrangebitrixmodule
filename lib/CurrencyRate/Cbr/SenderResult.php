<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

class SenderResult
{
    public string $response;
    public string $httpCode;
    public string $totalTime;

    public function setResponse(string $response): self
    {
        $this->response = $response;
        return $this;
    }

    public function setHttpCode(string $httpCode): self
    {
        $this->httpCode = $httpCode;
        return $this;
    }

    public function setTotalTime(string $totalTime): self
    {
        $this->totalTime = $totalTime;
        return $this;
    }
}
