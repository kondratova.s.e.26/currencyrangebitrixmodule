<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

use Bitrix\Main\Type\Date;
use Kondr\Currencyrate\CurrencyRate\CurrencyRate;
use Kondr\Currencyrate\CurrencyRate\Exceptions\EmptyRequiredFieldException;
use Kondr\Currencyrate\CurrencyRate\CurrencyRateRepository;

class CbrImporter
{
    public function importCourseOnToday()
    {
        try {
            $currentDate = new Date();
            $requestParams = (new CourseOnDateParamsBuilder)->build($currentDate);
            $senderResult = (new CurlSender)->send($requestParams);
            $currencyRates = XmlParser::parse($senderResult->response);

            foreach ($currencyRates as $currencyRate) {
                $this->importCurrencyRate($currencyRate, $currentDate);
            }

        } catch (\Throwable $exception) {
            //todo logger
        }
    }

    private function importCurrencyRate(CurrencyRateDTO $currencyRateDTO, Date $date): void
    {
        if (empty($currencyRateDTO->code) || empty($currencyRateDTO->course)) {
            throw new EmptyRequiredFieldException('Empty required field: code = ' . $currencyRateDTO->code
                . ', course = ' . $currencyRateDTO->course);
        }

        $currencyRate = (new CurrencyRate())
            ->setCode($currencyRateDTO->code)
            ->setDate($date)
            ->setCourse($currencyRateDTO->course);

        (new CurrencyRateRepository)->add($currencyRate);
    }
}
