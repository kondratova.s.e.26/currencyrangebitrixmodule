<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

use Kondr\Currencyrate\CurrencyRate\Exceptions\SenderException;
use Kondr\Currencyrate\CurrencyRate\Exceptions\HttpClientQueryException;

class CurlSender
{
    public function send(RequestParams $request): SenderResult
    {
        $startTime = microtime(true);
        try {
            $endTime = microtime(true);
            $result = $this->process($request);
            $result->setTotalTime($endTime - $startTime);
            return $result;
        } catch (\Exception $e) {
            $endTime = microtime(true);
            $senderResult = (new SenderResult())
                ->setTotalTime($endTime - $startTime);
            throw new SenderException($senderResult, 0, $e);
        }
    }

    protected function process(RequestParams $request): SenderResult
    {
        $requestFields = [
            CURLOPT_URL => $request->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $request->headers,
            CURLOPT_TIMEOUT => $request->streamTimeout,
            CURLOPT_CONNECTTIMEOUT => $request->socketTimeout,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $request->body,
        ];

        $curl = curl_init($request->url);
        curl_setopt_array($curl, $requestFields);
        $requestBody = curl_exec($curl);
        $result = self::makeResult($curl, $requestBody);
        curl_close($curl);

        return $result;
    }

    protected static function makeResult($curl, $requestBody): SenderResult
    {
        $curlInfo = curl_getinfo($curl);
        $httpCode = $curlInfo['http_code'];
        if ($requestBody === false) {
            throw new HttpClientQueryException(curl_error($curl));
        } elseif ($httpCode >= 400) {
            throw new HttpClientQueryException('HTTP Error: ' . $httpCode . '. ' . $requestBody);
        }

        return (new SenderResult())
            ->setResponse($requestBody)
            ->setHttpCode($httpCode);
    }
}
