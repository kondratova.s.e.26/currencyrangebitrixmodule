<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

class RequestParams
{
    public string $url;
    public array $headers = [];
    public string $body;
    public string $socketTimeout;
    public string $streamTimeout;
}
