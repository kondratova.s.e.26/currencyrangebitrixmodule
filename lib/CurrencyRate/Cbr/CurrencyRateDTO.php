<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

class CurrencyRateDTO
{
    public string $code;
    public float $course;
}
