<?php

namespace Kondr\Currencyrate\CurrencyRate\Cbr;

use Kondr\Currencyrate\CurrencyRate\Exceptions\XmlParserException;

class XmlParser
{
    public static function parse(string $xml): array
    {
        try {
            $parser = xml_parser_create();
            xml_set_element_handler($parser, [ self::class,'startTag'], [self::class, 'endTag']);
            xml_set_character_data_handler($parser, [ self::class,'contents']);

            global $kondrCurrencyRate, $kondrCurrentTag, $kondrOrder;

            $kondrCurrencyRate = [];
            $kondrCurrentTag = '';
            $kondrOrder = 0;

            xml_parse($parser, $xml);
            $result = $kondrCurrencyRate;

            unset($kondrCurrencyRate, $kondrCurrentTag, $kondrOrder);

            return $result;

        } catch (\Throwable $e) {
            throw new XmlParserException($e);
        }
    }

    protected static function contents($parser, $data)
    {
        global $kondrCurrentTag, $kondrCurrencyRate, $kondrOrder;

        if (!isset($kondrCurrencyRate[$kondrOrder])) {
            $kondrCurrencyRate[$kondrOrder] = new CurrencyRateDTO();
        }
        switch ($kondrCurrentTag) {
            case 'VCURS':
                $kondrCurrencyRate[$kondrOrder]->course = $data;
                break;
            case 'VCHCODE':
                $kondrCurrencyRate[$kondrOrder]->code = $data;
                break;
        }
    }

    protected static function startTag($parser, $name, $attrs)
    {
        global $kondrOrder, $kondrCurrentTag;
        if (isset($attrs['MSDATA:ROWORDER']) && $kondrOrder !== $attrs['MSDATA:ROWORDER']) {
            $kondrOrder = $attrs['MSDATA:ROWORDER'];
        }
        $kondrCurrentTag = $name;
    }

    protected static function endTag($parser, $name)
    {
    }
}
