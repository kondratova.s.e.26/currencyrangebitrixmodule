<?

namespace Kondr\Currencyrate\CurrencyRate;

use Bitrix\Main\ORM\Query\Filter\ConditionTree;
use \Bitrix\Main\UI\PageNavigation;
use Kondr\Currencyrate\CurrencyRateTable;
use Kondr\Currencyrate\CurrencyRate\Exceptions\CurrencyRateNotAddedException;
use Kondr\Currencyrate\CurrencyRate\Exceptions\CurrencyRateNotUpdatedException;
use Kondr\Currencyrate\CurrencyRate\Filter\RangeFilterInterface;
use Kondr\Currencyrate\CurrencyRate\Filter\ListFilterInterface;

class CurrencyRateRepository
{
    private array $select = ['*'];
    private array $sort = [];
    private array $filter = [];
    private PageNavigation | null $nav;
    private int | null $recordCount = null;

    /**
     * @return CurrencyRate[]
     *
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getAll(): array
    {
        $currencyRates = [];
        $filter = ConditionTree::createFromArray($this->filter);

        $query = CurrencyRateTable::query()
            ->setSelect($this->select)
            ->setOrder($this->sort)
            ->where($filter);

        if (isset($this->nav)) {
            $query
                ->countTotal(true)
                ->setOffset($this->nav->getOffset())
                ->setLimit($this->nav->getLimit());

            if (!$this->recordCount) {
                $this->recordCount = $query->queryCountTotal();
            }
        }
        $res = $query->exec();

        while ($fields = $res->fetch()) {
            $currencyRates[] = $this->buildCurrencyRate($fields);
        }

        return $currencyRates;
    }

    private function buildCurrencyRate(array $fields): CurrencyRate
    {
        $currencyRate = new CurrencyRate();

        $currencyRate->setId($fields['ID']);
        $currencyRate->setCode($fields['CODE'] ?? '');
        $currencyRate->setDate($fields['DATE'] ?? null);
        $currencyRate->setCourse($fields['COURSE'] ?? null);

        return $currencyRate;
    }

    public function setSelect(array $select): self
    {
        if (!!$select) {
            $this->select = array_unique(array_merge(['ID'], $select));
        }
        return $this;
    }

    public function setSort(Sort $sort): self
    {
        $this->sort = $sort->getSortOrder();
        return $this;
    }

    public function setFilter(array $filter): self
    {
        $this->filter = $filter;
        return $this;
    }

    public function setNav(PageNavigation $nav): self
    {
        $this->nav = $nav;
        return $this;
    }

    public function getRecordCount(): int
    {
        if (!!$this->recordCount) {
            return $this->recordCount;
        }

        if (!isset($this->nav)) {
            throw new \Exception('Навигация не установлена');
        }
        $filter = ConditionTree::createFromArray($this->filter);

        $query = CurrencyRateTable::query()
            ->setSelect($this->select)
            ->setOrder($this->sort)
            ->where($filter)
            ->countTotal(true)
            ->setOffset($this->nav->getOffset())
            ->setLimit($this->nav->getLimit());
        $this->recordCount = $query->queryCountTotal();

        return $this->recordCount;
    }

    public function setAdditionalFilter(array $filters): self
    {
        $where = [];

        foreach ($filters as $filter) {
                switch (true) {
                    case $filter instanceof RangeFilterInterface:
                        if (!empty($filter->getFrom())) {
                            $where[] = [
                                $filter->getName(),
                                '>=',
                                $filter->getFrom(),
                            ];
                        }
                        if (!empty($filter->getTo())) {
                            $where[] = [
                                $filter->getName(),
                                '<=',
                                $filter->getTo(),
                            ];
                        }
                        break;
                    case $filter instanceof ListFilterInterface:
                        if (!empty($filter->getSelected())) {
                            $where[] = [
                                $filter->getName(),
                                'in',
                                $filter->getSelected(),
                            ];
                        }
                        break;
                }
            }

        $this->filter = array_merge($this->filter, $where);

        return $this;
    }

    /**
     * @param CurrencyRate $currencyRate
     *
     * @return int
     *
     * @throws \Exception
     */
    public function add(CurrencyRate $currencyRate): int
    {
        $fields = $this->CurrencyRateToFields($currencyRate);
        $res = CurrencyRateTable::add($fields);
        if (!$res->isSuccess()) {
            throw new CurrencyRateNotAddedException();
        }

        return $res->getId();
    }

    /**
     * @param int $id
     * @param CurrencyRate $currencyRate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function update(int $id, CurrencyRate $currencyRate): void
    {
        $fields = $this->CurrencyRateToFields($currencyRate);
        $res = CurrencyRateTable::update($id, $fields);
        if (!$res->isSuccess()) {
            throw new CurrencyRateNotUpdatedException();
        }
    }

    private function CurrencyRateToFields(CurrencyRate $currencyRate): array
    {
        return [
            'CODE' => $currencyRate->getCode(),
            'DATE' => $currencyRate->getDate(),
            'COURSE' => $currencyRate->getCourse()
        ];
    }

    /**
     * @param int $id
     *
     * @return void
     *
     * @throws \Exception
     */
    public function delete(int $id): void
    {
        CurrencyRateTable::delete($id);
    }
}
