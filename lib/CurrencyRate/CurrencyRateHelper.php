<?php

namespace Kondr\Currencyrate\CurrencyRate;

use Bitrix\Main\UI\PageNavigation;
use Kondr\Currencyrate\CurrencyRate\Filter\CodeFilterFactory;
use Kondr\Currencyrate\CurrencyRate\Filter\CourseFilterFactory;
use Kondr\Currencyrate\CurrencyRate\Filter\DateFilterFactory;

class CurrencyRateHelper
{
    private array $arParams = [];
    private ?PageNavigation $nav = null;

    public function setParams(array $arParams = []): self
    {
        $this->arParams = $arParams;

        return $this;
    }

    public function getCurrencyRates():array
    {
        $sort = $this->getSort();
        $currencyRateRepository = (new CurrencyRateRepository())
            ->setSelect($this->arParams['DISPLAYED_COLUMNS_LIST'])
            ->setSort($sort)
            ->setAdditionalFilter($this->arParams['ADDITIONAL_FILTER']);

        if ($this->arParams['SHOW_NAV']) {
            $this->setNav($currencyRateRepository);
        }

        return $currencyRateRepository->getAll();
    }

    private function getSort(): Sort
    {
        return (new Sort())
            ->setSortBy1($this->arParams['SORT_BY1'])
            ->setSortOrder1($this->arParams['SORT_ORDER1'])
            ->setSortBy2($this->arParams['SORT_BY2'])
            ->setSortOrder2($this->arParams['SORT_ORDER2']);
    }

    private function setNav(CurrencyRateRepository $currencyRateRepository): void
    {
        $this->nav = (new PageNavigation($this->arParams['NAV_GET_PARAM']));
        $this->nav->setPageSize($this->arParams['PAGE_ELEMENT_COUNT']);
        $this->nav->allowAllRecords(true);

        $currencyRateRepository->setNav($this->nav);

        $recordCount = $currencyRateRepository->getRecordCount();
        $this->nav->setRecordCount($recordCount);
        $this->nav->initFromUri();
    }

    public function getNav(): ?PageNavigation
    {
        return $this->nav;
    }

    public function prepareAdditionalFilters(array $additionalFilters): array
    {
        $filters = [];

        foreach ($additionalFilters as $additionalFilter) {
            switch ($additionalFilter['name']) {
                case 'CODE':
                    $selected = $additionalFilter['values']['selected'];

                    if (!empty($selected)) {
                        $codeFactory = (new CodeFilterFactory())
                            ->setSelected($selected);
                        $filters[] = $codeFactory->createFilter();
                    }
                    break;
                case 'COURSE':
                    $from = $additionalFilter['values']['from'] ?? null;
                    $to   = $additionalFilter['values']['to'] ?? null;

                    if (!empty($from) || !empty($to)) {
                        $courseFactory = (new CourseFilterFactory())
                            ->setFrom($from)
                            ->setTo($to);
                        $filters[] = $courseFactory->createFilter();
                    }
                    break;
                case 'DATE':
                    $from = $additionalFilter['values']['from'] ?? null;
                    $to   = $additionalFilter['values']['to'] ?? null;

                    if (!empty($from) || !empty($to)) {
                        $dateFactory = (new DateFilterFactory())
                            ->setFrom($from)
                            ->setTo($to);
                        $filters[] = $dateFactory->createFilter();
                    }
                    break;
            }
        }

        return $filters;
    }

    public function prepareAjaxResponse(array $currencyRates): array
    {
        $ajaxResponse = [];

        foreach ($currencyRates as $currencyRate) {
            $ajaxResponse['list'][] = [
                'id' => $currencyRate->getId(),
                'code' => $currencyRate->getCode(),
                'date' => $currencyRate->getDateAsString(),
                'course' => $currencyRate->getCourse(),
            ];
        }

        return $ajaxResponse;
    }
}
