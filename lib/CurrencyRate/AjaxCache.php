<?php

namespace Kondr\Currencyrate\CurrencyRate;

use Kondr\Currencyrate\CurrencyRate\Filter\RangeFilterInterface;
use Kondr\Currencyrate\CurrencyRate\Filter\ListFilterInterface;

class AjaxCache
{
    private string $componentName = '';
    private string $cacheId = '';

    public function createCacheId(array $selectedFilters): self
    {
        $cacheId = 'kondrCurrencyRate';

        foreach ($selectedFilters as $selectedFilter) {
            switch (true) {
                case $selectedFilter instanceof RangeFilterInterface:
                    if (!!$selectedFilter->getFrom()) {
                        $cacheId .= $selectedFilter->getName() . 'from' . $selectedFilter->getFromAsString();
                    }
                    if (!!$selectedFilter->getTo()) {
                        $cacheId .= $selectedFilter->getName() . 'to' . $selectedFilter->getToAsString();
                    }
                    break;
                case $selectedFilter instanceof ListFilterInterface:
                    if (!!$selectedFilter->getSelected()) {
                        $cacheId .= $selectedFilter->getName() . 'in' . $selectedFilter->getSelectedAsString();
                    }
                    break;
            }
        }

        $this->cacheId = $cacheId;

        return $this;
    }

    public function setComponentName(string $componentName): self
    {
        $this->componentName = $componentName;
        return $this;
    }

    public function getPath(): string
    {
        $component = explode(':', $this->componentName);
        return '/' . SITE_ID . '/' . $component[0] . '/' . $component[1] . '/ajax';
    }

    public function getCacheId(): string
    {
        return $this->cacheId;
    }
}
