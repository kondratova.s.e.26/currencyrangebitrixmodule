<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

interface ListFilterInterface extends FilterInterface
{
    public function getAll(): array;
    public function setAll(array $all): self;
    public function setSelected(array $selected): self;
    public function getSelected(): array;
    public function getSelectedAsString(): string;
}
