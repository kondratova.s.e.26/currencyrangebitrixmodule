<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Request;

class FilterHelper
{
    public function getFilters(array $selectedFilters, array $displayedFilter): array
    {
        $filters = [];

        if (!$displayedFilter) {
            return $filters;
        }

        foreach ($selectedFilters as $selectedFilter) {
            switch (true) {
                case $selectedFilter instanceof RangeFilterInterface:
                    $filters[$selectedFilter->getName()] = (new FilterRepository())
                        ->setName($selectedFilter->getName())
                        ->setSelectedFilters($selectedFilters)
                        ->getRangeFilter();
                    break;
                case $selectedFilter instanceof ListFilterInterface:
                    $filters[$selectedFilter->getName()] = (new FilterRepository())
                        ->setName($selectedFilter->getName())
                        ->setSelectedFilters($selectedFilters)
                        ->getListFilter();
                    break;

            }
        }

        return $filters;
    }

    public function getSelectedFilters(array $displayedFilter): array
    {
        $selectedFilters = [];

        if (!$displayedFilter) {
            return $selectedFilters;
        }

        $request = Application::getInstance()->getContext()->getRequest();

        if (in_array('CODE', $displayedFilter)) {
            $selectedFilters['CODE'] = $this->getCodeFilter($request);
        }

        if (in_array('COURSE', $displayedFilter)) {
            $selectedFilters['COURSE'] = $this->getCourseFilter($request);
        }

        if (in_array('DATE', $displayedFilter)) {
            $selectedFilters['DATE'] = $this->getDateFilter($request);
        }

        return $selectedFilters;
    }

    private function getCodeFilter(Request|HttpRequest $request): ListFilterInterface
    {
        $code        = $request->get('code') ?? '';
        $codeFactory = (new CodeFilterFactory())->setSelected($code);

        return $codeFactory->createFilter();
    }

    private function getCourseFilter(Request|HttpRequest $request): RangeFilterInterface
    {
        $from = $request->get('courseFrom');
        $to   = $request->get('courseTo');

        $courseFactory = (new CourseFilterFactory())
            ->setFrom($from)
            ->setTo($to);

        return $courseFactory->createFilter();
    }

    private function getDateFilter(Request|HttpRequest $request): RangeFilterInterface
    {
        $from = $request->get('dateFrom');
        $to   = $request->get('dateTo');

        $dateFactory = (new DateFilterFactory())
            ->setFrom($from)
            ->setTo($to);;

        return $dateFactory->createFilter();
    }

    public function prepareAjaxResponse(array $filters): array
    {
        $ajaxFilters = [];

        foreach ($filters as $filter) {
            switch (true) {
                case $filter instanceof RangeFilterInterface:
                    $ajaxFilters[] = [
                        'name'   => $filter->getName(),
                        'type'   => 'range',
                        'values' => [
                            'from' => $filter->getFromAsString(),
                            'to'   => $filter->getToAsString()
                        ]
                    ];
                    break;
                case $filter instanceof ListFilterInterface:
                    $ajaxFilters[] = [
                        'name'   => $filter->getName(),
                        'type'   => 'list',
                        'values' => [
                            'all'      => $filter->getAll(),
                            'selected' => $filter->getSelected()
                        ]
                    ];
                    break;
            }
        }

        return $ajaxFilters;
    }
}
