<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

class CodeFilter implements ListFilterInterface
{
    private string $name;
    private string $factoryName;
    private array $all = [];
    private array $selected = [];

    public function setName(string $name): ListFilterInterface
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setFactoryName(string $factoryName): ListFilterInterface
    {
        $this->factoryName = $factoryName;
        return $this;
    }

    public function getFactoryName(): string
    {
        return $this->factoryName;
    }

    public function setAll(array $all): ListFilterInterface
    {
        $this->all = $all;
        return $this;
    }

    public function getAll(): array
    {
        return $this->all;
    }

    public function setSelected(array $selected): ListFilterInterface
    {
        $this->selected = $selected;
        return $this;
    }

    public function getSelected(): array
    {
        return $this->selected;
    }

    public function getSelectedAsString(): string
    {
        if (empty($this->selected)) {
            return '';
        }

        return implode('-OR-', $this->selected);
    }
}
