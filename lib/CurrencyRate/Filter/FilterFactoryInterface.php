<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

interface FilterFactoryInterface
{
    public function createFilter(): FilterInterface;
}
