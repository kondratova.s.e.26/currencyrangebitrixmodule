<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

class CourseFilter implements RangeFilterInterface
{
    private string $name;
    private string $factoryName;
    private ?float $from;
    private ?float $to;

    public function setName(string $name): RangeFilterInterface
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setFactoryName(string $factoryName): RangeFilterInterface
    {
        $this->factoryName = $factoryName;
        return $this;
    }

    public function getFactoryName(): string
    {
        return $this->factoryName;
    }

    public function setFrom($from): RangeFilterInterface
    {
        $this->from = $from;
        return $this;
    }

    public function getFrom(): ?float
    {
        return $this->from;
    }

    public function setTo($to): RangeFilterInterface
    {
        $this->to = $to;
        return $this;
    }

    public function getTo(): ?float
    {
        return $this->to;
    }

    public function getFromAsString(): string
    {
        return (string) $this->from ?? '';
    }

    public function getToAsString(): string
    {
        return (string) $this->to ?? '';
    }
}
