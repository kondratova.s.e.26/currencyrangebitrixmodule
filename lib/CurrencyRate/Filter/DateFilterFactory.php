<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

use Bitrix\Main\Type\Date;

class DateFilterFactory implements FilterFactoryInterface
{
    private ?Date $from;
    private ?Date $to;

    public function setFrom(string | Date | null $from): DateFilterFactory
    {
        if (is_string($from) && $this->validate($from)) {
            $from = new Date($from);
        }

        $this->from = $from instanceof Date ? $from : null;

        return $this;
    }

    public function setTo(string | Date | null $to): DateFilterFactory
    {
        if (is_string($to) && $this->validate($to)) {
            $to = new Date($to);
        }

        $this->to = $to instanceof Date ? $to : null;

        return $this;
    }

    private function validate(string $string): bool
    {
        $chr_en = "0-9.";
        return preg_match("/^[$chr_en]+$/", $string);
    }

    public function createFilter(): RangeFilterInterface
    {
        return (new DateFilter())
            ->setName('DATE')
            ->setFactoryName(get_class($this))
            ->setFrom($this->from)
            ->setTo($this->to);
    }
}
