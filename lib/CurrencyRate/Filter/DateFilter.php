<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

use Bitrix\Main\Type\Date;

class DateFilter implements RangeFilterInterface
{
    private string $name;
    private string $factoryName;
    private ?Date $from;
    private ?Date $to;

    public function setName(string $name): RangeFilterInterface
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setFactoryName(string $factoryName): RangeFilterInterface
    {
        $this->factoryName = $factoryName;
        return $this;
    }

    public function getFactoryName(): string
    {
        return $this->factoryName;
    }

    public function setFrom($from): RangeFilterInterface
    {
        $this->from = $from;
        return $this;
    }

    public function getFrom(): ?Date
    {
        return $this->from;
    }

    public function getFromAsString(): string
    {
        return $this->from ? $this->from->toString() : '';
    }

    public function setTo($to): RangeFilterInterface
    {
        $this->to =$to;
        return $this;
    }

    public function getTo(): ?Date
    {
        return $this->to;
    }

    public function getToAsString(): string
    {
        return $this->to ? $this->to->toString() : '';
    }
}
