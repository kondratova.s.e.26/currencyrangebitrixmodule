<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

class CourseFilterFactory implements FilterFactoryInterface
{
    private ?float $from = null;
    private ?float $to = null;

    public function setFrom(string | float | null $from): CourseFilterFactory
    {
        if (is_string($from) && !$this->validate($from)) {
            $from = null;
        }

        $this->from = $from;

        return $this;
    }

    public function setTo(string | float | null $to): CourseFilterFactory
    {
        if (is_string($to) && !$this->validate($to)) {
            $to = null;
        }

        $this->to   = $to;

        return $this;
    }

    private function validate(string $string): bool
    {
        if (empty($string)) {
            return false;
        }

        $chr_en = "0-9.";
        return preg_match("/^[$chr_en]+$/", $string);
    }

    public function createFilter(): RangeFilterInterface
    {
        return (new CourseFilter())
            ->setName('COURSE')
            ->setFactoryName(get_class($this))
            ->setFrom($this->from)
            ->setTo($this->to);
    }
}
