<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

class CodeFilterFactory implements FilterFactoryInterface
{
    private array $all = [];
    private array $selected = [];

    public function setAll(array $all): self
    {
        $this->all = $all;
        return $this;
    }

    public function setSelected(string | array | null $selected): self
    {
        if (is_string($selected) && $this->validate($selected)) {
            $selected = explode('-OR-', strtoupper($selected));
        }

        $this->selected = is_array($selected) ? $selected : [];

        return $this;
    }

    private function validate(string $string): bool
    {
        if (empty($string)) {
            return false;
        }

        $chr_en = "a-zA-Z-";
        return preg_match("/^[$chr_en]+$/", $string);
    }

    public function createFilter(): ListFilterInterface
    {
        return (new CodeFilter())
            ->setName('CODE')
            ->setFactoryName(get_class($this))
            ->setAll($this->all)
            ->setSelected($this->selected);
    }
}
