<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

interface FilterInterface
{
    public function setName(string $name): self;
    public function getName(): string;
    public function setFactoryName(string $factoryName): self;
    public function getFactoryName(): string;
}
