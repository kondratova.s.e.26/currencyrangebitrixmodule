<?php

namespace Kondr\Currencyrate\CurrencyRate\Filter;

use Bitrix\Main\Type\Date;

interface RangeFilterInterface extends FilterInterface
{
    public function setFrom(Date | float | null $from): self;
    public function getFrom(): Date | float | null;
    public function getFromAsString(): string;
    public function setTo(Date | float | null $to): self;
    public function getTo(): Date | float | null;
    public function getToAsString(): string;
}
