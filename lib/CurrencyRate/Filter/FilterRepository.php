<?

namespace Kondr\Currencyrate\CurrencyRate\Filter;

use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\ORM\Query\Filter\ConditionTree;
use Kondr\Currencyrate\CurrencyRateTable;
use Kondr\Currencyrate\CurrencyRate\Sort;

class FilterRepository
{
    private string $name;
    private array $sort = [];
    private array $selectedFilters = [];

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getRangeFilter()
    {
        $factoryName = $this->selectedFilters[$this->name]->getFactoryName();
        $factory = new $factoryName();
        $min = $this->selectedFilters[$this->name]->getFrom();
        $max = $this->selectedFilters[$this->name]->getTo();

        $query = CurrencyRateTable::query()
            ->setSelect(['MIN', 'MAX'])
            ->setOrder($this->sort)
            ->registerRuntimeField(new ExpressionField('MIN', 'MIN(%s)', $this->name))
            ->registerRuntimeField(new ExpressionField('MAX', 'MAX(%s)', $this->name));

        $filter = $this->prepareFilter();
        $query->where($filter);

        $res = $query->exec();

        while ($fields = $res->fetch()) {
            if (!$min || $min < $fields['MIN']) {
                $min = $fields['MIN'];
            }
            if (!$max || $max > $fields['MAX']) {
                $max = $fields['MAX'];
            }
        }
        $factory->setFrom($min);
        $factory->setTo($max);

        return $factory->createFilter();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getListFilter()
    {
        $factoryName = $this->selectedFilters[$this->name]->getFactoryName();
        $factory = new $factoryName();
        $values = [];

        $query = CurrencyRateTable::query()
            ->setSelect([$this->name])
            ->setDistinct()
            ->setOrder($this->sort);

        $filter = $this->prepareFilter();
        $query->where($filter);

        $res = $query->exec();

        while ($fields = $res->fetch()) {
            $values[] = $fields[$this->name];
        }

        $selected = array_intersect($this->selectedFilters[$this->name]->getSelected(), $values);

        $factory->setSelected($selected);
        $factory->setAll($values);

        return $factory->createFilter();
    }

    private function prepareFilter(): ConditionTree
    {
        $where = [];
        foreach ($this->selectedFilters as $selectedFilter) {
            if ($selectedFilter->getName() === $this->name) {
                continue;
            }

            switch (true) {
                case $selectedFilter instanceof RangeFilterInterface:
                    if ($selectedFilter->getFrom()) {
                        $where[] = [
                            $selectedFilter->getName(),
                            '>=',
                            $selectedFilter->getFrom(),
                        ];
                    }
                    if ($selectedFilter->getTo()) {
                        $where[] = [
                            $selectedFilter->getName(),
                            '<=',
                            $selectedFilter->getTo(),
                        ];
                    }
                    break;
                case $selectedFilter instanceof ListFilterInterface:
                    if ($selectedFilter->getSelected()) {
                        $where[] = [
                            $selectedFilter->getName(),
                            'in',
                            $selectedFilter->getSelected(),
                        ];
                    }
                    break;
            }
        }

        return ConditionTree::createFromArray($where);
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setSelectedFilters(array $selectedFilters): self
    {
        $this->selectedFilters = $selectedFilters;
        return $this;
    }

    public function setSort(Sort $sort): self
    {
        $this->sort = $sort->getSortOrder();
        return $this;
    }
}
