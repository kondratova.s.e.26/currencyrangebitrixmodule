<?

namespace Kondr\Currencyrate\CurrencyRate\Exceptions;

class CurrencyRateNotUpdatedException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct("The currency exchange rate has not been updated");
    }
}
