<?php

namespace Kondr\Currencyrate\CurrencyRate\Exceptions;

use Kondr\Currencyrate\CurrencyRate\Cbr\SenderResult;
use Throwable;

class SenderException extends \RuntimeException
{
    public SenderResult $senderResult;

    public function __construct(SenderResult $senderResult, $code = 0, Throwable $previous = null)
    {
        parent::__construct("", $code, $previous);
        $this->senderResult = $senderResult;
    }
}
