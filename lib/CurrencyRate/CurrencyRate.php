<?php

namespace Kondr\Currencyrate\CurrencyRate;
use Bitrix\Main\Type\Date;

class CurrencyRate
{
    private int $id;
    private string $code;
    private ?Date $date;
    private ?float $course;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function getDateAsString(): string
    {
        return $this->date->toString();
    }

    public function setDate(?Date $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function getCourse(): ?float
    {
        return $this->course;
    }

    public function setCourse(?float $course): self
    {
        $this->course = $course;
        return $this;
    }
}
