<?php

namespace Kondr\Currencyrate\CurrencyRate;

/**
 * Аякс ответ клиенту
 * ```php
 * [
 *     'success'        => true|false,
 *     'errors'         => [errorField => errorMsg],
 *     'notifyMessages' => array of messages shown in notificator,
 *     'redirect'       => URL for redirect,
 *     'payload'        => any custom data
 * ]
 * ```
 */
class AjaxResponse implements \JsonSerializable
{
    protected bool $success = true;
    protected array $errors = [];
    protected array $notifyMessages = [];
    protected string $redirect = '';
    protected mixed $payload = null;

    protected function __construct()
    {
    }

    /**
     * @return static успешный ответ клиенту
     */
    public static function getSuccessResponse(): self
    {
        $response = new static();
        $response->success = true;

        return $response;
    }

    /**
     * Ответ клиенту в случае ошибки
     *
     * @return AjaxResponse
     */
    public static function getFailureResponse(): self
    {
        $response = new static();
        $response->success = false;

        return $response;
    }

    /**
     * Добавляет ошибки к ответу
     *
     * @param array $errors ['errorCode' => 'errorMessage']
     *
     * @return AjaxResponse
     */
    public function withErrors(array $errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * Добавляет уведомления для показа в нотификаторе
     *
     * @param array | string $messages
     *
     * @return AjaxResponse
     */
    public function withNotifyMessages(array | string $messages): self
    {
        $this->notifyMessages = (array)$messages;
        return $this;
    }

    /**
     * Добавляет URL для редиректа
     *
     * @param string $redirectUrl
     *
     * @return AjaxResponse
     */
    public function withRedirect(string $redirectUrl): self
    {
        $this->redirect = $redirectUrl;
        return $this;
    }

    /**
     * Добавляет данные к ответу
     *
     * @param mixed $payload
     *
     * @return AjaxResponse
     */
    public function withPayload(mixed $payload): self
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        $result = [
            'success' => !!$this->success
        ];

        if ($this->errors) {
            $result['errors'] = $this->errors;
        }

        if ($this->notifyMessages) {
            $result['notifyMessages'] = $this->notifyMessages;
        }

        if ($this->redirect) {
            $result['redirect'] = $this->redirect;
        }

        if ($this->payload !== null) {
            $result['payload'] = $this->payload;
        }

        return $result;
    }
}
