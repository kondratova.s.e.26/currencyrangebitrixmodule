<?

namespace Kondr\Currencyrate\CurrencyRate;

class Sort
{
    private ?string $sortBy1 = null;
    private ?string $sortOrder1 = null;
    private ?string $sortBy2 = null;
    private ?string $sortOrder2 = null;

    public function setSortBy1(?string $sortBy1): Sort
    {
        $this->sortBy1 = $sortBy1;
        return $this;
    }

    public function setSortOrder1(?string $sortOrder1): Sort
    {
        $this->sortOrder1 = $sortOrder1;
        return $this;
    }

    public function setSortBy2(?string $sortBy2): Sort
    {
        $this->sortBy2 = $sortBy2;
        return $this;
    }

    public function setSortOrder2(?string $sortOrder2): Sort
    {
        $this->sortOrder2 = $sortOrder2;
        return $this;
    }

    public function getSortOrder(): array
    {
        $sortOrder = [];

        if ($this->sortBy1) {
            $sortOrder[$this->sortBy1] = $this->sortOrder1;
        }
        if ($this->sortBy2) {
            $sortOrder[$this->sortBy2] = $this->sortOrder2;
        }

        return $sortOrder;
    }
}
