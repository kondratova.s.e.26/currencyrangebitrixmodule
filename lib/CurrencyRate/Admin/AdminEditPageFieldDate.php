<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminEditPageFieldDate extends AdminEditPageField
{
    public function __construct(
        string $id,
        string $title = '',
        string $value = '',
        bool $required = false,
        bool $hide = false
    ) {
        $this->type = 'date';
        parent::__construct($id, $title, $value, $required, $hide);
    }

    public function validate(): bool
    {
        $chr_en = "0-9.";
        return preg_match("/^[$chr_en]+$/", $this->value) || !$this->value;
    }

    public function getRow():string
    {
        $calendar = CalendarDate($this->id, htmlspecialcharsbx($this->value), $this->id, 20);
        return <<<HTML
	<tr class="adm-detail-required-field">
		<td width="40%">{$this->title}:</td>
		<td width="60%">{$calendar} <b style="color: red">{$this->error}</b></td>
	</tr>
HTML;
    }
}
