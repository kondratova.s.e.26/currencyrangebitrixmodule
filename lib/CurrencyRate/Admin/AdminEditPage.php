<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

use Bitrix\Main\Application;
use Bitrix\Main\DB\Exception;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Request;
use Bitrix\Main\Entity\DataManager;
use CAdminTabControl;
use CAdminMessage;
use CAdminContextMenu;
use Bitrix\Main\Type\Date;

class AdminEditPage
{
    protected ?int $id = null;
    protected array $fields = [];
    protected array $tabs = [];
    protected array $menu = [];
    /** @var CAdminMessage[] $errors */
    protected array $errors;
    protected string $right = 'D';
    protected string $editPage = '';
    protected string $listPage = '';
    protected string $addPageTitle = '';
    protected string $editPageTitle = '';
    protected string $saveErrorText = '';
    protected string $emptyFieldErrorText = '';
    protected string $invalidFieldErrorText = '';
    protected DataManager $ormTable;
    protected CAdminTabControl $tabControl;
    protected CAdminContextMenu $contextMenu;
    protected Request | HttpRequest $request;

    public function __construct()
    {
        $this->request = Application::getInstance()->getContext()->getRequest();
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function setFields(array $fields): self
    {
        $this->fields = $fields;
        return $this;
    }

    public function setRight(string $right): self
    {
        $this->right = $right;
        return $this;
    }

    public function setEditPage(string $editPage): self
    {
        $this->editPage = $editPage;
        return $this;
    }

    public function setListPage(string $listPage): self
    {
        $this->listPage = $listPage;
        return $this;
    }

    public function setEditPageTitle(string $editPageTitle): self
    {
        $this->editPageTitle = $editPageTitle;
        return $this;
    }

    public function setAddPageTitle(string $addPageTitle): self
    {
        $this->addPageTitle = $addPageTitle;
        return $this;
    }

    public function setSaveErrorText(string $saveErrorText): self
    {
        $this->saveErrorText = $saveErrorText;
        return $this;
    }

    public function setEmptyFieldErrorText(string $emptyFieldErrorText): self
    {
        $this->emptyFieldErrorText = $emptyFieldErrorText;
        return $this;
    }

    public function setInvalidFieldErrorText(string $invalidFieldErrorText): self
    {
        $this->invalidFieldErrorText = $invalidFieldErrorText;
        return $this;
    }

    public function setOrmTable(DataManager $ormTable): self
    {
        $this->ormTable = $ormTable;
        return $this;
    }

    public function setTabs(array $tabs): self
    {
        $this->tabs = $tabs;
        return $this;
    }

    public function setMenu(array $menu): self
    {
        $this->menu = $menu;
        return $this;
    }

    protected function initTabControl(): void
    {
        $this->tabControl = new CAdminTabControl('editTab', $this->tabs);
    }

    protected function intContextMenu(): void
    {
        $this->contextMenu = new CAdminContextMenu($this->menu);
    }

    public function save(): void
    {
        if (!$this->canBeSaved()) {
            return;
        }

        try {
            if (!empty($this->id)) {
                $this->updateItem();
            } else {
                $this->addItem();
            }
        } catch (Exception $e) {
            $this->errors[] = new CAdminMessage($e->getMessage());
        }

        if (empty($this->errors)) {
            if ($this->isApplyAction()) {
                LocalRedirect('/bitrix/admin/' . $this->editPage . '.php?ID=' . $this->id
                    . '&mess=ok&lang=' . LANG . '&' . $this->tabControl->ActiveTabParam());
            } else {
                LocalRedirect('/bitrix/admin/' . $this->listPage . '.php?lang=' . LANG);
            }
        }
    }

    protected function canBeSaved(): bool
    {
        return $this->request->isPost()
            && ($this->isSaveAction() || $this->isApplyAction())
            && $this->right >= 'W'
            && check_bitrix_sessid();
    }

    protected function isApplyAction(): bool
    {
        return !(empty($this->request->getPost('apply')));
    }

    protected function isSaveAction(): bool
    {
        return !(empty($this->request->getPost('save')));
    }

    protected function fillFieldsFromDB(): void
    {
        $select = !empty($this->fields) ? array_keys($this->fields) : ['*'];
        $res = $this->ormTable::query()
            ->setFilter(['ID' => $this->id])
            ->setSelect($select)
            ->exec();

        if (!$fields = $res->fetch()) {
            $this->id = null;
            if(isset($this->fields['ID'])){
                $this->fields['ID']->setHide(true);
            }
            return;
        }

        foreach ($fields as $key => $value) {
            if($value instanceof Date){
                $value = $value->toString();
            }
            if (empty($this->fields[$key]->getValue())) {
                $this->fields[$key]->setValue($value);
            }
        }
    }

    protected function updateItem(): void
    {
        if (!$this->checkFields()) {
            return;
        }

        $res = $this->ormTable::update($this->id, $this->getPreparedFields());

        if (!$res->isSuccess()) {
            throw new Exception($this->saveErrorText);
        }
    }

    protected function addItem(): void
    {
        if (!$this->checkFields()) {
            return;
        }

        $res = $this->ormTable::add($this->getPreparedFields());

        if (!$res->isSuccess()) {
            throw new Exception($this->saveErrorText);
        }

        $this->id = $res->getId();
    }

    protected function checkFields(): bool
    {
        $ok = true;

        foreach ($this->fields as $field) {
            if ($field->getId() === 'ID') {
                continue;
            }

            if ($field->isEmpty()) {
                $field->setError($this->emptyFieldErrorText);
                $ok = false;
                continue;
            }
            if (!$field->validate()) {
                $field->setError($this->invalidFieldErrorText );
                $ok = false;
            }
        }

        return $ok;
    }

    protected function getPreparedFields(): array
    {
        $fields = [];

        foreach ($this->fields as $field) {
            if ($field->getType() === 'date') {
                $fields[$field->getId()] = new Date($field->getValue());
            } elseif ($field->getId() !== 'ID') {
                $fields[$field->getId()] = $field->getValue();
            }
        }

        return $fields;
    }

    protected function setTitle(): void
    {
        if (empty($this->id)) {
            $title = $this->addPageTitle;
        } else {
            $title = str_replace("#ID#", $this->id, $this->editPageTitle);
        }

        global  $APPLICATION;
        $APPLICATION->SetTitle($title);
    }

    public function init()
    {
        $this->setTitle();
        $this->initTabControl();
        $this->save();
        $this->intContextMenu();

        if ($this->id > 0) {
            $this->fillFieldsFromDB();
        }

    }

    public function show()
    {
        $this->contextMenu->Show();

        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                echo $error->Show();
            }
        }

        global $APPLICATION;
        echo '<form name="form" action="' . $APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '" method="POST">';
        echo bitrix_sessid_post();
        $this->tabControl->Begin();
        $this->tabControl->BeginNextTab();

        foreach ($this->fields as $field) {
            if (!$field->getHide()) {
                echo $field->getRow();
            }
        }

        $this->tabControl->Buttons([
            'disabled' => ($this->right < 'W'),
            'back_url' => $this->listPage . '.php?lang=' . LANGUAGE_ID
        ]);
        $this->tabControl->End();
        echo '<input type="hidden" name="ID" value="' . $this->id . '"></form>';
    }
}
