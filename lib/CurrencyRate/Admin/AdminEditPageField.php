<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminEditPageField
{
    protected string $id = '';
    protected string $title = '';
    protected string $value = '';
    protected string $error = '';
    protected string $type = '';
    protected bool $required = false;
    protected bool $hide = false;

    public function __construct(
        string $id,
        string $title = '',
        string $value = '',
        bool $required = false,
        bool $hide = false
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->value = $value;
        $this->required = $required;
        $this->hide = $hide;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setHide(bool $hide): self
    {
        $this->hide = $hide;
        return $this;
    }

    public function getHide(): bool
    {
        return $this->hide;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setError(string $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function isEmpty(): bool
    {
        if(!$this->required && empty($this->value)) {
            return false;
        }

        return empty($this->value);
    }

    public function validate(): bool
    {
        $chr_en = "a-zA-Z0-9-.";
        return preg_match("/^[$chr_en]+$/", $this->value) || !$this->value;
    }

    public function getRow():string
    {
        return <<<HTML
	<tr>
		<td>{$this->title}</td>
		<td>{$this->value}</td>
	</tr>
HTML;
    }
}
