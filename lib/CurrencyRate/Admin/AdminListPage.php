<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Type\Date;
use CAdminSorting;
use CAdminList;
use CAdminFilter;
use CAdminResult;
use Bitrix\Main\ORM\Query\Result;
use Kondr\Currencyrate\CurrencyRateTable;

class AdminListPage
{
    protected array $menu = [];
    /**  @var AdminListColumn[] $columns */
    protected array $columns = [];
    protected string $tableID = '';
    protected string $pageTitle = '';
    protected string $navLabel = '';
    protected string $dateFormatError = '';
    protected string $saveError = '';
    protected string $deleteError = '';
    protected string $invalidFieldError = '';
    protected string $editAction = '';
    protected string $deleteAction = '';
    protected string $deleteActionConf = '';
    protected string $right = 'D';
    protected string $editPage = '';
    protected DataManager $ormTable;
    protected CAdminSorting $sort;
    protected CAdminList $list;
    protected CAdminResult $data;

    public function setMenu(array $menu): self
    {
        $this->menu = $menu;
        return $this;
    }

    public function setPageTitle(string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;
        return $this;
    }

    public function setNavLabel(string $navLabel): self
    {
        $this->navLabel = $navLabel;
        return $this;
    }

    public function setDateFormatError(string $dateFormatError): self
    {
        $this->dateFormatError = $dateFormatError;
        return $this;
    }

    public function setSaveError(string $saveError): self
    {
        $this->saveError = $saveError;
        return $this;
    }

    public function setDeleteError(string $deleteError): self
    {
        $this->deleteError = $deleteError;
        return $this;
    }

    public function setInvalidFieldError(string $invalidFieldError): self
    {
        $this->invalidFieldError = $invalidFieldError;
        return $this;
    }

    public function setEditAction(string $editAction): self
    {
        $this->editAction = $editAction;
        return $this;
    }

    public function setDeleteAction(string $deleteAction): self
    {
        $this->deleteAction = $deleteAction;
        return $this;
    }

    public function setDeleteActionConf(string $deleteActionConf): self
    {
        $this->deleteActionConf = $deleteActionConf;
        return $this;
    }

    public function setRight(string $right): self
    {
        $this->right = $right;
        return $this;
    }

    public function setEditPage(string $editPage): self
    {
        $this->editPage = $editPage;
        return $this;
    }

    public function setOrmTable(DataManager $ormTable): self
    {
        $this->ormTable = $ormTable;
        return $this;
    }

    public function addColumn(AdminListColumn $column): void
    {
        $this->columns[$column->id] = $column;
    }

    public function init()
    {
        global $APPLICATION;

        $this->initTableId();
        $this->initSort();
        $this->initList();
        $this->initFilter();
        $this->saveAction();
        $this->editAction();
        $this->list->addHeaders($this->getHeaders());

        $select = $this->getSelectedFields();

        $this->initData($select);
        $this->initNav();
        $this->initRows($select);

        $this->list->AddFooter($this->getFooter());
        $this->list->AddGroupActionTable($this->getGroupActions());
        $this->list->AddAdminContextMenu($this->menu);
        $this->list->CheckListMode();
        $APPLICATION->SetTitle($this->pageTitle);

        global /** @noinspection PhpUnusedLocalVariableInspection */
        $adminPage, $adminMenu, $adminChain, $USER;
    }

    public function show(): void
    {
        $this->displayFilter();
        $this->list->DisplayList();
    }

    protected function initTableId(): void
    {
        $this->tableID = 'tbl' . $this->ormTable::getTableName();
    }

    protected function initSort(): void
    {
        $this->sort = new CAdminSorting($this->tableID, 'ID', 'desc');
    }

    protected function initList(): void
    {
        $this->list = new CAdminList($this->tableID, $this->sort);
    }

    protected function initFilter()
    {
        $filters = [
            'find',
            'find_type',
        ];
        foreach ($this->columns as $column)
        {
            if (isset($column->info['filter']))
                $filters[] = $column->info['filter'];
        }

        $this->list->InitFilter($filters);

        if (!empty($errors = $this->checkFilter())) {
            $this->list->AddFilterError($errors);
        }
    }

    protected function checkFilter(): string
    {
        $errors = '';

        $filter = $this->list->getFilter();
        if (isset($filter) && isset($filter['find']) && $filter['find_type'] === 'date') {
            $dateOk = MkDateTime(FmtDate($filter['find'],'D.M.Y'),'d.m.Y');

            if (!$dateOk){
                $errors.= $this->dateFormatError . '<br>';
            }
        }

        return $errors;
    }

    protected function validateFields($fields): bool
    {
        foreach ($fields as $id => $field) {
            if (!$this->columns[$id]->validate($field)) {
                return false;
            }
        }

        return true;
    }

    protected function saveAction()
    {
        if ($this->list->EditAction() && $this->right >= 'W') {
            foreach ($this->list->GetEditFields() as $id => $fields) {
                if (!$this->validateFields($fields)) {
                    $this->list->AddGroupError($this->invalidFieldError, $id);
                    continue;
                }

                $preparedFields = $this->getPreparedFields($fields);
                if (!$this->dataUpdate($id, $preparedFields)) {
                    $this->list->AddGroupError($this->saveError, $id);
                }
            }
        }
    }

    protected function getPreparedFields(array $fields): array
    {
        if (!empty($fields['DATE'])) {
            $fields['DATE'] = new Date($fields['DATE']);
        }

        return $fields;
    }

    protected function dataUpdate(int $id, array $fields): bool
    {
        return (CurrencyRateTable::update($id, $fields))->isSuccess();
    }

    protected function editAction(): void
    {
        if (($ids = $this->list->GroupAction()) && $this->right == 'W') {
            if ($this->list->IsGroupActionToAll()) {
                $queryResult = $this->getDataSource($this->getOrder(), $this->getFilter(), ['ID']);
                while ($fields = $queryResult->fetch()) {
                    $ids[] = $fields['ID'];
                }
            }

            $action = $this->list->GetAction();
            foreach($ids as $id)
            {
                if (strlen($id) <= 0) {
                    continue;
                }

                if ($action === 'delete' && !$this->dataDelete($id)) {
                    $this->list->AddGroupError($this->deleteError, $id);
                }
            }
        }
    }

    protected function getOrder(): array
    {
        global $by, $order;
        return [$by => $order];
    }

    protected function getFilter(): array
    {
        global $find, $find_type;

        $filters = [];
        foreach ($this->columns as $column)
        {
            if (isset($column->info['filter']) && isset($column->info['filter_key'])) {
                if (isset($column->info['find_type']) && !empty($find) && $find_type == $column->info['find_type']) {
                    $filters[$column->info['filter_key']] = $find;
                } elseif (isset($GLOBALS[$column->info['filter']])) {
                    $filters[$column->info['filter_key']] = $GLOBALS[$column->info['filter']];
                }
            }
        }

        foreach ($filters as $key => $value) {
            if (empty($value)) {
                unset($filters[$key]);
            }
        }

        return $filters;
    }

    protected function dataDelete(int $id): bool
    {
        return ($this->ormTable::delete($id))->isSuccess();
    }

    protected function getHeaders(): array
    {
        $arHeaders = [];

        foreach ($this->columns as $column) {
            $arHeaders[] = [
                'id'      => $column->id,
                'content' => $column->info['content'],
                'sort'    => $column->info['sort'],
                'align'   => $column->info['align'],
                'default' => $column->info['default'],
            ];
        }

        return $arHeaders;
    }

    protected function getDataSource(array $order = [], array $filter = [], array $select = []): Result
    {
        if (!empty($this->list->arFilterErrors)) {
            $filter = [];
        }

        return $this->ormTable::query()
            ->setSelect($select)
            ->setFilter($filter)
            ->setOrder($order)
            ->exec();
    }

    protected function initData(array $select): void
    {
        $queryResult = $this->getDataSource($this->getOrder(), $this->getFilter(), $select);
        $this->data = new CAdminResult($queryResult, $this->tableID);
    }

    protected function getSelectedFields()
    {
        $arSelectedFields = $this->list->GetVisibleHeaderColumns();
        if (!is_array($arSelectedFields) || empty($arSelectedFields)) {
            $arSelectedFields = [];
            foreach ($this->columns as $column) {
                if ($column->info['default'])
                    $arSelectedFields[] = $column->id;
            }
        }

        return $arSelectedFields;
    }

    protected function initNav(): void
    {
        $this->data->NavStart();
        $this->list->NavText($this->data->GetNavPrint($this->navLabel));
    }

    protected function initRows(array $select): void
    {

        while ($res = $this->data->NavNext(true, 'f_'))
        {
            $row = $this->list->AddRow($res['ID'], $res);
            foreach ($select as $fieldId)
            {
                $column = $this->columns[$fieldId];
                if ($column)
                {
                    switch ($column->fieldType) {
                        case 'view':
                            $row->AddField($fieldId, $res[$column->id]);
                            break;
                        case 'input':
                            $row->AddInputField($fieldId);
                            break;
                        case 'calendar':
                            $row->AddCalendarField($fieldId);
                            break;
                    }
                }
            }

            $row->AddActions($this->getActions($res['ID']));
        }
    }

    protected function getActions($id): array
    {
        $actions = [];
        $actions[] = [
            'ICON' => 'edit',
            'DEFAULT' => true,
            'TEXT' => $this->editAction,
            'ACTION' => $this->list->ActionRedirect($this->editPage . '.php?ID=' . $id)
        ];

        if ($this->right >= 'W') {
            $actions[] = ['SEPARATOR' => true];
            $actions[] = [
                'ICON' => 'delete',
                'TEXT' => $this->deleteAction,
                'ACTION' => "if(confirm('" . $this->deleteActionConf . "')) "
                    . $this->list->ActionDoGroup($id, 'delete')
            ];
        }

        return $actions;
    }

    protected function getFooter(): array
    {
        return [
            [
                'title' => getMessage('MAIN_ADMIN_LIST_SELECTED'),
                'value' => $this->data->SelectedRowsCount()
            ],
            [
                'counter' => true,
                'title' => getMessage('MAIN_ADMIN_LIST_CHECKED'),
                'value' => 0
            ],
        ];
    }

    protected function getGroupActions(): array
    {
        return [
            'delete' => getMessage('MAIN_ADMIN_LIST_DELETE')
        ];
    }

    protected function displayFilter(): void
    {
        global $APPLICATION, $find, $find_type;

        $findFilter = [
            'reference'    => [],
            'reference_id' => [],
        ];
        $listFilter = [];
        foreach ($this->columns as $column) {
            if (isset($column->info['filter'])) {
                $listFilter[$column->info['filter']] = $column->info['content'];
                if (isset($column->info['find_type'])) {
                    $findFilter['reference'][] = $column->info['content'];
                    $findFilter['reference_id'][] = $column->info['find_type'];
                }
            }
        }

        if (!empty($listFilter))
        {
            $filter = new CAdminFilter($this->tableID . '_filter', $listFilter);
            ?>
            <form name="find_form" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
                <? $filter->Begin(); ?>
                <? if (!empty($findFilter['reference'])): ?>
                    <tr>
                        <td><b><?=GetMessage('PERFMON_HIT_FIND')?>:</b></td>
                        <td><input
                                type="text" size="25" name="find"
                                value="<? echo htmlspecialcharsbx($find) ?>"><? echo SelectBoxFromArray('find_type', $findFilter, $find_type, '', ''); ?>
                        </td>
                    </tr>
                <? endif; ?>
                <?
                foreach ($this->columns as $column)
                {
                    if (isset($column->info['filter']))
                    {
                        ?>
                        <tr>
                        <td><? echo $column->info['content'] ?></td>
                        <td><? echo $column->getFilterInput() ?></td>
                        </tr><?
                    }
                }
                $filter->Buttons(array(
                    'table_id' => $this->sTableID,
                    'url' => $APPLICATION->GetCurPage(),
                    'form' => 'find_form',
                ));
                $filter->End();
                ?>
            </form>
            <?
        }
    }
}
