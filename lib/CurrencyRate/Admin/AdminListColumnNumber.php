<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminListColumnNumber extends AdminListColumn
{
    function __construct(string $id, string $fieldType, array $info, bool $required = false)
    {
        $info["align"] = "right";
        $info['find_type'] = 'number';

        parent::__construct($id, $fieldType, $info, $required);
    }

    public function isEmpty(): bool
    {
        if(!$this->required && empty($this->value)) {
            return false;
        }

        return empty($this->value);
    }

    public function validate(string $value = ''): bool
    {
        $chr_en = "0-9.";
        return preg_match("/^[$chr_en]+$/", $value) || !$value;
    }
}
