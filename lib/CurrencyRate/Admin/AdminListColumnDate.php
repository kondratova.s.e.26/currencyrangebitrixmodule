<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminListColumnDate extends AdminListColumn
{
    function __construct(string $id, string $fieldType, array $info, bool $required = false)
    {
        $info['align'] = 'right';
        $info['find_type'] = 'date';

        parent::__construct($id, $fieldType, $info, $required);
    }

    public function validate(string $value = ''): bool
    {
        $chr_en = "0-9.";
        return preg_match("/^[$chr_en]+$/", $value) || !$value;
    }
}
