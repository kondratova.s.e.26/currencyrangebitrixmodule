<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminListColumn
{
    public string $id        = '';
    public string $fieldType = '';
    public array  $info      = [];
    protected bool $required = false;

    public function __construct(string $id, string $fieldType, array $info, bool $required = false)
    {
        $this->id = $id;
        $this->fieldType = $fieldType;
        $this->info = $info;
        $this->required = $required;
    }

    public function getFilterInput(): string
    {
        return '<input type="text" name="' . $this->info['filter'] . '" size="47" value="'
            . htmlspecialcharsbx($GLOBALS[$this->info['filter']]) . '">';
    }

    public function validate(string $value = ''): bool
    {
        $chr_en = "a-zA-Z0-9-.";
        return preg_match("/^[$chr_en]+$/", $value) || !$value;
    }
}
