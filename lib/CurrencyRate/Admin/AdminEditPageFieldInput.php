<?php

namespace Kondr\Currencyrate\CurrencyRate\Admin;

class AdminEditPageFieldInput extends AdminEditPageField
{
    public function __construct(
        string $id,
        string $title = '',
        string $value = '',
        bool $required = false,
        bool $hide = false
    ) {
        parent::__construct($id, $title, $value, $required, $hide);
    }

    public function validate(): bool
    {
        $chr_en = "a-zA-Z0-9";
        return preg_match("/^[$chr_en]+$/", $this->value) || !$this->value;
    }

    public function getRow():string
    {
        return <<<HTML
	<tr class="adm-detail-required-field">
		<td>{$this->title}</td>
		<td><input type="text" name="{$this->id}" size="40" value="{$this->value}"> <b style="color: red">{$this->error}</b> </td>
	</tr>
HTML;
    }
}
